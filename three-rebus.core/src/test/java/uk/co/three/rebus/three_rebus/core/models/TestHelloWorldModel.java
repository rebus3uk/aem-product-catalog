/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package uk.co.three.rebus.three_rebus.core.models;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import junitx.util.PrivateAccessor;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;

import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.OfferingsDetails;

/**
 * Simple JUnit test verifying the HelloWorldModel
 */
@RunWith(MockitoJUnitRunner.class)
public class TestHelloWorldModel {

    //@Inject
   // private HelloWorldModel hello;
    @Mock
	OfferingsDetailsProvider offeringsDetailsProvider;
    @Mock
    SlingSettingsService settings;
    @Mock
    OfferingsDetails offer;
    
   // private String slingId;
    
    @Test
    public void setup() throws Exception {
    	String  slingId = "43";
        when(settings.getSlingId()).thenReturn(slingId);

        /*hello = new HelloWorldModel();
        PrivateAccessor.setField(hello, "settings", settings);
        hello.init();*/
       // String msg = hello.getMessage();
        //  assertNotNull(msg);
         //  assertTrue(msg.length() > 0);
       // when(offer.get)
        
       // offer=offeringsDetailsProvider.getOfferingsDetails();
         //  when(offeringsDetailsProvider.getOfferingsDetails()).thenReturn(offer);
         //  assertNotNull(offeringsDetailsProvider.getOfferingsDetails());
           
           
           
         /*  Integer catalog=Integer.parseInt(offeringsDetailsProvider.getOfferingsDetails().getProductOfferingRelationship().get(0).getCatalogId());
           System.out.println(catalog);
           
           when(catalog).thenReturn(43);*/

           // use mock in test....
         //  assertEquals(catalog, new Integer(45));
    }
    
   // @Test
    public void testGetMessage() throws Exception {
        // some very basic junit tests
        String msg = "gfgf";
     //  assertNotNull(msg);
      //  assertTrue(msg.length() > 0);
        
      //  assertNotNull(offeringsDetailsProvider.getOfferingsDetails());
        
      //  Integer catalog=Integer.parseInt(offeringsDetailsProvider.getOfferingsDetails().getProductOfferingRelationship().get(0).getCatalogId());
       // System.out.println(catalog);
        
       // when(catalog).thenReturn(43);

        // use mock in test....
      //  assertEquals(catalog, new Integer(45));
    }

}
