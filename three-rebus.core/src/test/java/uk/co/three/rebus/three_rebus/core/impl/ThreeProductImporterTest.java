package uk.co.three.rebus.three_rebus.core.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.mockito.Mockito.*;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import epc.h3g.com.CustomerAccountType;
import epc.h3g.com.OfferingsDetails;
import epc.h3g.com.ProductOfferingSpecification;
import uk.co.three.rebus.three_rebus.core.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.request.ProductOfferRequest;
import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.write.CatalogInfomationWriter;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ThreeProductImporter.class})
public class ThreeProductImporterTest {
	@Mock
	private OfferingsDetailsProvider offeringsDetailsProviderMock;
	@Mock
	private QueryProvider queryProviderImpMock;
	@Mock
	private ResourceResolverFactory resourceFactory;
	@Mock
	private Session session;
	@Mock
	private Node node;
	@Mock
	private Node productNode;
	@Mock
	private Resource resource;
	@Mock
	private ResourceResolver resourceResolver;

	private String absolutePath = "/content/three_rebus/products";

	private String storeName = "catalogOne";
	@Mock
	private ProductDataGetRequest productDataGetRequest;
	@Mock
	private CatalogInfomationWriter catalogInfomationWriter;

	@Mock
	private ProductOfferRequest productOfferRequest;

	ProductDataGetResponse productDataRepsonse;

	ThreeProductImporter threeProductImporter;
	
	SessionHandlerImpl handlerImpl;

	@Before
	public void setUp() throws Exception {
		setProductDataResponse();
		when(resourceFactory.getServiceResourceResolver(any(Map.class))).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
		when(resourceResolver.getResource(absolutePath)).thenReturn(resource);
		when(resource.adaptTo(Node.class)).thenReturn(node);
		when(node.hasNode("catalogOne")).thenReturn(true);
		when(session.getNode(absolutePath+"/" + storeName)).thenReturn(node);	
		PowerMockito.whenNew(CatalogInfomationWriter.class).withNoArguments().thenReturn(catalogInfomationWriter);
		PowerMockito.whenNew(ProductDataGetRequest.class).withNoArguments().thenReturn(productDataGetRequest);
		PowerMockito.whenNew(ProductOfferRequest.class).withNoArguments().thenReturn(productOfferRequest);
		when(offeringsDetailsProviderMock.getOfferingsDetails(productDataGetRequest)).thenReturn(productDataRepsonse);
		when(catalogInfomationWriter.offerDetailsProcessor(productOfferRequest)).thenReturn(productDataRepsonse);
		threeProductImporter = new ThreeProductImporter();
		handlerImpl = new SessionHandlerImpl();
		Field rFactory = SessionHandlerImpl.class.getDeclaredField("resourceFactory");
		rFactory.setAccessible(true);
		rFactory.set(handlerImpl, resourceFactory);
		Field offeringsDetailsField = ThreeProductImporter.class.getDeclaredField("offeringsDetailsProvider");
		offeringsDetailsField.setAccessible(true);
		offeringsDetailsField.set(threeProductImporter, offeringsDetailsProviderMock);
		Field queryProviderImpField = ThreeProductImporter.class.getDeclaredField("queryProviderImp");
		queryProviderImpField.setAccessible(true);
		queryProviderImpField.set(threeProductImporter, queryProviderImpMock);
	
	}

	private void setProductDataResponse() {
		productDataRepsonse =  new ProductDataGetResponse();
		productDataRepsonse.setCatalogNode(node);
		OfferingsDetails offeringsDetails = Mockito.mock(OfferingsDetails.class);
		ProductOfferingSpecification productOfferingSpecification = new ProductOfferingSpecification();
		CustomerAccountType accountType = new CustomerAccountType();
		accountType.setCatalogId("AT_INDIVIDUAL");
		accountType.setName("Individual account");
		productOfferingSpecification.setAccountType(accountType);
		List<ProductOfferingSpecification> posList = new ArrayList<ProductOfferingSpecification>();
		posList.add(productOfferingSpecification);
		when(offeringsDetails.getProductOfferingSpecification()).thenReturn(posList);
		productDataRepsonse.setOfferingsDetails(offeringsDetails);
	}

	@Test
	public void checkPositiveFlowWithProduct() {
	//	Assert.assertNotNull();
	//doNothing().when(threeProductImporter).doImport();
		Assert.assertTrue(threeProductImporter.getProductDataResponse().hasErrors());
	}

	@Test
	public void checkPositiveFlowWithStore() throws RepositoryException {
		when(node.hasNode("catalogOne")).thenReturn(false);
		when(session.getNode(absolutePath)).thenReturn(productNode);
		when(productNode.addNode(storeName)).thenReturn(node);
		when(node.getPath()).thenReturn(absolutePath+"/" + storeName);
		//Assert.assertNotNull(threeProductImporter.doImport().getOfferingsDetails());
	}

	@Test
	public void checkNegativeFlowWithLoginExeption() throws org.apache.sling.api.resource.LoginException {
		when(resourceFactory.getServiceResourceResolver(any(Map.class))).thenThrow(org.apache.sling.api.resource.LoginException.class);
		//Assert.assertTrue(threeProductImporter.doImport().hasErrors());
	}
	
	@Test
	public void checkNegativeFlowWithRespositoryExeption() throws RepositoryException {
		when(node.hasNode("catalogOne")).thenThrow(RepositoryException.class);	
		//Assert.assertTrue(threeProductImporter.doImport().hasErrors());
	}




}
