package uk.co.three.rebus.three_rebus.core.models;


import java.util.Locale;
import java.util.ResourceBundle;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Matchers.any;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;


@RunWith(PowerMockRunner.class)
@PrepareForTest({FetchErrorMessage.class, ResourceBundle.class}) 
public class FetchErrorMessageTest {
	
	@Mock
	ResourceBundle errorMessageBundleMock;
	
	//FetchErrorMessage fetchErrorMessage;
	@Before
	public void setUp() {
		PowerMockito.mockStatic(ResourceBundle.class);
		Mockito.when(ResourceBundle.getBundle(any(String.class), any(Locale.class))).thenReturn(errorMessageBundleMock);
		when(errorMessageBundleMock.getString(any(String.class))).thenReturn("somevalue");
		//fetchErrorMessage =  new FetchErrorMessage();
	}
	
	@Test
	public void checkPositive() {
		Assert.assertNotNull(FetchErrorMessage.getErrorMessage("message"));
		Assert.assertTrue(FetchErrorMessage.getErrorMessage("message").equals("somevalue"));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void checkNegative() {
		when(errorMessageBundleMock.getString(any(String.class))).thenThrow(NullPointerException.class);
		Assert.assertTrue(FetchErrorMessage.getErrorMessage("message").isEmpty());
	}

}

