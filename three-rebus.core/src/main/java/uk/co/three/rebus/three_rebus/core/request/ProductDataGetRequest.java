package uk.co.three.rebus.three_rebus.core.request;

import javax.jcr.Session;

/**
 * Specfic Request class for getting offering details request in project
 * 
 * @author GS00355198
 *
 */

public class ProductDataGetRequest extends BaseRequest {

	/*
	 * JCR Session Object connect to AEM repository
	 * 
	 * 
	 * 
	 */
	private Session session;

	private String filePath;

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}
