/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.response;

import java.util.List;

import epc.h3g.com.ProductOfferingSpecification;

/**
 * This is the response class for getting all the products
 * This class will contain the list of product offering specifications
 * @author DD00493288
 *
 */
public class GetAllProductsResponse extends BaseResponse {

	
	private List<ProductOfferingSpecification> products = null;

	/**
	 * @return the products
	 */
	public List<ProductOfferingSpecification> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<ProductOfferingSpecification> products) {
		this.products = products;
	}
	
	
}
