package uk.co.three.rebus.three_rebus.core.service;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.InvalidQueryException;

import epc.h3g.com.ProductOfferingSpecification;
import uk.co.three.rebus.three_rebus.core.models.ProductOfferingNodeData;
import uk.co.three.rebus.three_rebus.core.request.GetAllProductsRequest;
import uk.co.three.rebus.three_rebus.core.response.GetAllProductsResponse;


/**
 * This interface provides the read operations of the product catalog module
 * @author DD00493288
 *
 */
public interface QueryProvider {

	public List<ProductOfferingSpecification> getProductOfferSample(Session session)
			throws InvalidQueryException, RepositoryException;
	

	
//public ProductOfferingSpecification getProductOfferSample(Session session, String productName) throws InvalidQueryException, RepositoryException;


//arpitha
//public ProductOfferingSpecification getProductOfferDetails2(Session session, String productName) throws  RepositoryException;


	/**
	 * This method gets all the products
	 * @param request
	 * @return
	 */
	public GetAllProductsResponse getAllProducts(GetAllProductsRequest request);


}
