/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import epc.h3g.com.OfferingsDetails;
import uk.co.three.rebus.three_rebus.core.error.ErrorObject;

/**
 * This is the base class for all responses in project All individual response
 * classes should extend from this class
 * 
 * @author GS00355198
 *
 */
public class BaseResponse implements Serializable {

	public List<ErrorObject> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<ErrorObject> errorList) {
		this.errorList = errorList;
	}

	private List<ErrorObject> errorList = new ArrayList<ErrorObject>();

	/**
	 * 
	 * @param errorCode
	 * @param errorMessage
	 */
	public void addErrorObject(int errorCode, String errorMessage) {
		ErrorObject errorObject = new ErrorObject(errorCode, errorMessage);
		addErrorObject(errorObject);
	}

	/**
	 * 
	 * @param errorObject
	 */
	public void addErrorObject(ErrorObject errorObject) {
		errorList.add(errorObject);
	}

	/**
	 * This method returns true if errors are present
	 * 
	 * @return
	 */
	public boolean hasErrors() {
		return !errorList.isEmpty();
	}

}
