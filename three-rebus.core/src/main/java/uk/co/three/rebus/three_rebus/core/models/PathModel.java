/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.models;

import javax.jcr.Node;

/**
 * @author MS00506946
 *
 */
public class PathModel {

	private String name;

	private Node pathNode;

	private String absolutePath;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pathNode
	 */
	public Node getPathNode() {
		return pathNode;
	}

	/**
	 * @param pathNode
	 *            the pathNode to set
	 */
	public void setPathNode(Node pathNode) {
		this.pathNode = pathNode;
	}

	/**
	 * @return the absolutePath
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}

	/**
	 * @param absolutePath
	 *            the absolutePath to set
	 */
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

}
