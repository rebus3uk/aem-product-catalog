package uk.co.three.rebus.three_rebus.core.service;

import uk.co.three.rebus.three_rebus.core.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;

public interface OfferingsDetailsProvider {

	public ProductDataGetResponse getOfferingsDetails(ProductDataGetRequest productDataGetRequest);

}
