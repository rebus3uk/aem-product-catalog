package uk.co.three.rebus.three_rebus.core.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.CommercialExport;
import epc.h3g.com.OfferingsDetails;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.util.CommonUtils;

@Component(label = "Title", description = "Description.", immediate = true, metatype = true) // ,
																								// policy
																								// =
																								// ConfigurationPolicy.REQUIRE
@Service(value = { OfferingsDetailsProvider.class })

/**
 * This class is responsible handling xml file reading and providing product
 * catalog data object's.
 * 
 * @author
 *
 */
public class OfferingsDetailsProviderImpl implements OfferingsDetailsProvider {

	private static final Logger log = LoggerFactory.getLogger(OfferingsDetailsProviderImpl.class);
	
	private Map<Integer,String> errorMap = new HashMap<Integer,String>();

	public OfferingsDetails getOfferingsDetails() {

		return null;
	}

	@Activate
	protected void activate(Map<String, String> config) {
		log.info("OfferingsDetailsProviderImpl ACTIVATED");

	}
	/**
	 * This method is responsible for handling xml parsing
	 * 
	 * Passing productDataGetResponse
	 * 
	 * @param productDataGetResponse
	 *            object contains xml file stream
	 * 
	 * @return OfferingsDetails we get JAXB object .
	 * 
	 */
	private OfferingsDetails populateOfferingsDetails(InputStream stream) {
		XMLInputFactory xif = XMLInputFactory.newFactory();
		XMLStreamReader xsr = null;
		try {
			xsr = xif.createXMLStreamReader(stream);
			xsr.nextTag();
			while (!xsr.getLocalName().equals("commercialExport")) {

				xsr.nextTag();
			}
		} catch (XMLStreamException e1) {
			log.info("XMLException:" + e1.getMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(CommercialExport.class);
			log.info("context:: " + context.toString());
		} catch (JAXBException e) {
			log.info("JAxB problem:"+e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}

		if (context == null) {
			log.info("Failed to create context");
			return null;
		}

		Unmarshaller unmarshaller = null;
		try {
			unmarshaller = context.createUnmarshaller();
		} catch (JAXBException e) {
			log.info("JAxB problem:"+e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}

		if (unmarshaller == null) {
			log.info("failed to create unmarshaller instance");
			return null;
		}

		JAXBElement<CommercialExport> jbElement = null;
		try {
			jbElement = unmarshaller.unmarshal(xsr, CommercialExport.class);

			log.info(jbElement.getName().getNamespaceURI());

		} catch (JAXBException e) {
			log.info("JAxB problem:"+e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}

		if (jbElement == null) {
			log.info("Failed to read the xml file");
			return null;
		}

		log.info("Success in reading xml file content");
		CommercialExport value = jbElement.getValue();
		OfferingsDetails offeringsDetails = value.getOfferingsDetails();

		return offeringsDetails;

	}

	/**
	 * This method is responsible for providing xml file data
	 * 
	 * Passing productDataGetRequest
	 * 
	 * @param productDataGetRequest
	 *            object which contains file path and session data .
	 * 
	 * @return ProductDataGetResponse object which is product catalog data
	 * 
	 */

	@Override
	public ProductDataGetResponse getOfferingsDetails(ProductDataGetRequest productDataGetRequest) {

		OfferingsDetails offeringsDetailsObj = null;
		InputStream stream = null;
		ProductDataGetResponse productDataGetResponse = new ProductDataGetResponse();

		try {
			ClassLoader classLoader = getClass().getClassLoader();
			String result = IOUtils.toString(classLoader.getResourceAsStream("abc.xml"));
			stream = new ByteArrayInputStream(result.getBytes());
			
		} catch (Exception e) {
			log.info("JAxB problem:"+e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}

		if(stream != null) {

			offeringsDetailsObj = populateOfferingsDetails(stream);

			if (offeringsDetailsObj == null) {

				productDataGetResponse.addErrorObject(ErrorCodeConstants.PRODUCT_OFFER_EMPTY,
						 FetchErrorMessage.getErrorMessage(ErrorCodeConstants.PRODUCT_OFFER_EMPTY_MESSAGE));

			} else if (offeringsDetailsObj.getProductOfferingSpecification().isEmpty()) {

				productDataGetResponse.addErrorObject(ErrorCodeConstants.PRODUCT_OFFER_LIST_EMPTY,
						 FetchErrorMessage.getErrorMessage(ErrorCodeConstants.PRODUCT_OFFER_LIST_EMPTY_MESSAGE));

			} else {

				productDataGetResponse.setOfferingsDetails(offeringsDetailsObj);
			}
		}
		CommonUtils.setErrorMessages(productDataGetResponse, errorMap);

		return productDataGetResponse;
	}

}
