package uk.co.three.rebus.three_rebus.core.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.qom.Constraint;
import javax.jcr.query.qom.QueryObjectModel;
import javax.jcr.query.qom.QueryObjectModelFactory;
import javax.jcr.query.qom.Selector;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.value.StringValue;

import epc.h3g.com.ProductOfferingSpecification;
import uk.co.three.rebus.three_rebus.core.models.ProductOfferingNodeData;
import uk.co.three.rebus.three_rebus.core.request.GetAllProductsRequest;
import uk.co.three.rebus.three_rebus.core.response.GetAllProductsResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.service.ProductImporterService;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

@Component(label = "Title", description = "Description.", immediate = true, metatype = true)
@Service(value = { QueryProvider.class })
public class QueryProviderImp implements QueryProvider {

	@Override
	public List<ProductOfferingSpecification> getProductOfferSample(
			Session session) throws RepositoryException {

		List<ProductOfferingSpecification> productOfferingSpecification = new ArrayList<ProductOfferingSpecification>();

		QueryManager queryManager = session.getWorkspace().getQueryManager();

		Query query = queryManager.createQuery(RebusUtils.PRODUCT_OFFERING,
				Query.JCR_SQL2);
		QueryResult result = query.execute();

		NodeIterator nodeIterator = result.getNodes();
		while (nodeIterator.hasNext()) {
			productOfferingSpecification.add(getAllProperties(nodeIterator
					.nextNode()));

		}

		return productOfferingSpecification;
	}

	private ProductOfferingSpecification getAllProperties(Node node) {

		try {
			ProductOfferingSpecification productOfferingSpecification;
			NodeIterator nodeitertor = node.getNodes();
			while (nodeitertor.hasNext()) {
				PropertyIterator propertyIterator = nodeitertor.nextNode()
						.getProperties();
				while (propertyIterator.hasNext()) {
					productOfferingSpecification = (ProductOfferingSpecification) propertyIterator
							.nextProperty();

				}
				getAllProperties(nodeitertor.nextNode());

			}
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		return productOfferingSpecification;
	}

	@Override
	public GetAllProductsResponse getAllProducts(GetAllProductsRequest request) {
		// initialize the response
		GetAllProductsResponse response = new GetAllProductsResponse();
		List<ProductOfferingSpecification> productOfferingSpecification = new ArrayList<ProductOfferingSpecification>();
		response.setProducts(productOfferingSpecification);

		Session session = request.getSession();
		QueryManager queryManager = session.getWorkspace().getQueryManager();

		Query query = queryManager.createQuery(RebusUtils.PRODUCT_OFFERING,
				Query.JCR_SQL2);
		QueryResult result = query.execute();

		NodeIterator nodeIterator = result.getNodes();
		while (nodeIterator.hasNext()) {
			productOfferingSpecification.add(getAllProperties(nodeIterator
					.nextNode()));

		}
		
		
		
		return response;
	}

}
