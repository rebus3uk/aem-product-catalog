package uk.co.three.rebus.three_rebus.core.request;

import java.io.Serializable;

import org.apache.sling.api.SlingHttpServletRequest;

public class CatalogRequest extends BaseRequest implements Serializable {

	private String resourcePath = "";

	public CatalogRequest() {

	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

}
