package uk.co.three.rebus.three_rebus.core.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.request.ProductOfferRequest;
import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.service.ProductImporterService;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.util.CommonUtils;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;
import uk.co.three.rebus.three_rebus.core.write.CatalogInfomationWriter;

/**
 * This class is responsible for providing utilite's to handle product catalog
 * in repository .
 * 
 * @author MS00506946
 *
 */

@Component(immediate = true)
@Service(value = { ProductImporterService.class })
public class ThreeProductImporter extends SessionHandlerImpl implements ProductImporterService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	private OfferingsDetailsProvider offeringsDetailsProvider;

	@Reference
	private QueryProvider queryProviderImp;
	protected String ABSOLUTE_PATH = "/content/three_rebus/products";
	protected String STORE_NAME = "catalogOne";
	private Map<Integer, String> errorMap = new HashMap<Integer, String>();

	@Override
	@Activate
	public void doImport() {
		getProductDataResponse();
	}

	@Override
	public ProductDataGetResponse getProductDataResponse() {
		Session session = null;
		String productPath = null;
		ProductDataGetResponse productDataGetResponse = new ProductDataGetResponse();
		try {
			ResourceResolver resourceResolver = getResourceResolver();
			session = openSession();
			Resource resource = resourceResolver.getResource(ABSOLUTE_PATH);
			Node storeRoot = resource.adaptTo(Node.class);
			if (!storeRoot.hasNode(STORE_NAME)) {
				addStoreName(session, ABSOLUTE_PATH);
			} else {
				productPath = ABSOLUTE_PATH + "/" + STORE_NAME;
				addProduct(session, productPath);
			}
			CommonUtils.setErrorMessages(productDataGetResponse, errorMap);
			session.logout();
		} catch (LoginException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.LOGIN_EXEPTION,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.LOGIN_EXEPTION_MESSAGE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
		return productDataGetResponse;
	}

	/**
	 * This method will add store name into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,absolutePath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param absolutePath
	 *            shows exact location where operation need to perform .
	 * 
	 * @return void
	 * 
	 */

	private void addStoreName(Session session, String absolutePath) {
		try {
			Node productsNode = session.getNode(absolutePath);
			Node storeNode = productsNode.addNode(STORE_NAME);
			storeNode.setProperty(RebusUtils.TYPE, RebusUtils.STORE);
			String productPath = storeNode.getPath();
			addProduct(session, productPath);
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
	}

	/**
	 * This method will add product into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,productPath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param productPath
	 *            shows exact location where product need to be stored .
	 * 
	 * @return void
	 * 
	 */

	private void addProduct(Session session, String productPath) {
		try {
			CatalogInfomationWriter catalogInfomationWriter = new CatalogInfomationWriter();
			Node catalogNode = session.getNode(productPath);
			ProductDataGetRequest productDataGetRequest = new ProductDataGetRequest();
			productDataGetRequest.setSession(session);
			ProductDataGetResponse productDataGetResponse = offeringsDetailsProvider
					.getOfferingsDetails(productDataGetRequest);
			ProductOfferRequest productOfferRequest = new ProductOfferRequest();
			if (!productDataGetResponse.hasErrors()) {
				productOfferRequest.setCatalogNode(catalogNode);
				productOfferRequest.setProductDataGetResponse(productDataGetResponse);
				productOfferRequest.setSession(session);
				productDataGetResponse = catalogInfomationWriter.offerDetailsProcessor(productOfferRequest);
				session.save();
			}
		} catch (PathNotFoundException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.PATHNOTFOUND_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.PATHNOTFOUND_FAILURE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
	}
}
