/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.write;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.AddressLocalization;
import epc.h3g.com.AvailabilityRule;
import epc.h3g.com.Category;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CompatibilityRule;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.CustomerAccountType;
import epc.h3g.com.Localization;
import epc.h3g.com.Location;
import epc.h3g.com.PaymentMethod;
import epc.h3g.com.PricePlanSpecification;
import epc.h3g.com.ProductOfferingSpecification;
import epc.h3g.com.ProductSpecification;
import epc.h3g.com.RelationshipSpecification;
import epc.h3g.com.Specification;
import epc.h3g.com.SupplierPartner;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.models.PathModel;
import uk.co.three.rebus.three_rebus.core.request.ProductOfferRequest;
import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.util.CommonUtils;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

/**
 * This class is responsible for handling Catalog data and inserting the catalog
 * data into repository . Its subclass of SpecificationWriter .
 * 
 * @author MS00506946
 *
 */
public class CatalogInfomationWriter extends SpecificationWriter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private Map<String, String> referencedSpecPathMap = null;
	private List<PathModel> pathDataList = null;

	public ProductDataGetResponse offerDetailsProcessor(ProductOfferRequest productOfferRequest) {
		ProductDataGetResponse productDataGetResponse = productOfferRequest.getProductDataGetResponse();
		Node catalogNode = productOfferRequest.getCatalogNode();
		List<ProductOfferingSpecification> productOfferingSpecification = productDataGetResponse.offeringsDetails
				.getProductOfferingSpecification();
		productOfferingSpecification.stream().filter(productOfferSpec -> productOfferSpec != null)
				.forEach(productOfferSpec -> {
					try {
						referencedSpecPathMap = new HashMap<String, String>();
						pathDataList = new ArrayList<PathModel>();
						List<Category> categoryList = productOfferSpec.getCategory();
						writeCategory(catalogNode, categoryList, productOfferSpec, referencedSpecPathMap, pathDataList);
						referencedSpecification(catalogNode, productOfferSpec, referencedSpecPathMap, pathDataList);
						updatePathInOfferDetails(pathDataList, referencedSpecPathMap);
					} catch (Exception e) {
						log.info("Error:" + e.getLocalizedMessage());
						productDataGetResponse.addErrorObject(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
								FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
					}
				});

		productDataGetResponse.setCatalogNode(catalogNode);
		CommonUtils.setErrorMessages(productDataGetResponse, errorMap);
		return productDataGetResponse;
	}

	/**
	 * This method is used to update the path details inside offerDetails node
	 * 
	 * Passing referencedMap ,pathModel ,pathList
	 * 
	 * @param referencedMap
	 *            shows exact location where product need to be stored .
	 * @param pathModel
	 *            is used to holding the information of the node where path
	 *            needs to update. .
	 * @param pathList
	 *            is used to get the path details which is reflecting in node
	 *            property
	 * 
	 * @return void
	 * 
	 */

	private void updatePathInOfferDetails(List<PathModel> pathList, Map<String, String> referencedMap) {
		pathList.stream().filter(pathModel -> pathModel != null).forEach(pathModel -> {
			referencedMap.entrySet().forEach(referenced -> {
				if (pathModel.getName().equalsIgnoreCase(referenced.getKey())) {
					try {
						pathModel.getPathNode().setProperty(RebusUtils.PRODUCT_REFERENCE_PATH, referenced.getValue());
					} catch (Exception e) {
						log.info("Error:" + e.getLocalizedMessage());
						errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
								FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
					}
				}
			});
		});
	}

	/**
	 * This method is used to add the ProductOffer Node and set the respective
	 * properties like Product_Offerings_Status,Is_Activated etc .
	 * 
	 * Passing categoryNode ,productOfferSpec ,referencedSpecPathMap
	 * ,pathDataLis
	 * 
	 * @param categoryNode
	 *            used to add the property .
	 * @param productOfferSpec
	 *            is used to hold the Product offer data .
	 * @param referencedSpecPathMap
	 *            is used to hold the reference specification node absolute path
	 *            .
	 * @param pathDataList
	 *            is used to hold the exact location of reference specification
	 *            node where we need to set the absolute path of reference
	 *            specification .
	 * 
	 *            return void
	 * 
	 */

	private void createProductOfferNode(Node categoryNode, ProductOfferingSpecification productOfferSpec,
			Map<String, String> referencedSpecPathMap, List<PathModel> pathDataList) {
		try {
			Node productOfferNode = null;
			if (!categoryNode.hasNode(productOfferSpec.getName())) {
				productOfferNode = categoryNode.addNode(productOfferSpec.getName(), RebusUtils.NT_UNSTRUCTURED);
				referencedSpecPathMap.put(productOfferSpec.getName(), productOfferNode.getPath());
			} else {
				productOfferNode = categoryNode.getNode(productOfferSpec.getName());
			}
			entitySpecificationSetProperty(productOfferNode, productOfferSpec);
			//7/6
			//productOfferNode.setProperty(RebusUtils.PRODUCT_OFFERINGS_STATUS, productOfferSpec.getStatus().value());
			productOfferNode.setProperty(RebusUtils.IS_ACTIVATED, productOfferSpec.isActivation());
			productOfferNode.setProperty(RebusUtils.IS_ADDON, productOfferSpec.isAddon());
			productOfferNode.setProperty(RebusUtils.IS_COMMERICAL, productOfferSpec.isCommercial());
			productOfferNode.setProperty(RebusUtils.IS_COMPLEX, productOfferSpec.isComplex());
			productOfferNode.setProperty(RebusUtils.IS_SHAREABLE, productOfferSpec.isShareable());
			productOfferingSpec(productOfferSpec, productOfferNode, pathDataList);
			specificationSetProperty(productOfferNode, productOfferSpec);
		} catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));
		} catch (LockException lockEx) {
			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));
		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		} catch (Exception ex) {
			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}
	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * referencedSpecification .
	 * 
	 * Passing catalogNode ,productOfferSpec ,referencedSpecPathMap
	 * ,pathDataList
	 * 
	 * @param catalogNode
	 *            used to add the property .
	 * @param productOfferSpec
	 *            is used to hold the product offer data .
	 * @param referencedSpecPathMap
	 *            is used to hold the reference specification node absolute
	 *            path.
	 * @param pathDataList
	 *            is used to hold the exact location of reference specification
	 *            node where we need to set the absolute path of reference
	 *            specification .
	 * 
	 * @return void
	 * 
	 */

	private void referencedSpecification(Node catalogNode, ProductOfferingSpecification productOfferSpec,
			Map<String, String> referencedSpecPathMap, List<PathModel> pathDataList) {
		productOfferSpec.getReferencedSpecification().stream()
				.filter(relationshipSpecification -> relationshipSpecification != null)
				.forEach(relationshipSpecification -> {
					for (Specification specification : relationshipSpecification.getRelatedSpecification()) {
						ProductOfferingSpecification sp = (ProductOfferingSpecification) specification;
						writeCategory(catalogNode, sp.getCategory(), sp, referencedSpecPathMap, pathDataList);
					}
				});
	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * ProductOfferingSpecification like city,Country etc .
	 * 
	 * Passing productOfferSpec ,productOfferNode ,pathDataList
	 * 
	 * @param productOfferSpec
	 *            is used to hold the product offer data .
	 * @param productOfferNode
	 *            is used to get the ProductOfferingSpecification object to
	 *            productOfferNode object.
	 * @param pathDataList
	 *            is used to hold the exact location of reference specification
	 *            node where we need to set the absolute path of reference
	 *            specification .
	 * 
	 * @return void
	 * 
	 */

	private void productOfferingSpec(ProductOfferingSpecification productOfferSpec, Node productOfferNode,
			List<PathModel> pathDataList) {
		try {
			if (productOfferSpec.getPaymentMethod() != null) {
				for (PaymentMethod paymentMethod : productOfferSpec.getPaymentMethod()) {
					Node paymentMethodNode = productOfferNode.addNode(paymentMethod.getName(),
							RebusUtils.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(paymentMethodNode, paymentMethod);
				}
			}
			if (!productOfferSpec.getCompatibilityRule().isEmpty()) {
				for (CompatibilityRule compatibilityRule : productOfferSpec.getCompatibilityRule()) {
					Node compatibilityRuleNode = productOfferNode.addNode(compatibilityRule.getName());
					entitySpecificationSetProperty(compatibilityRuleNode, compatibilityRule);
					ConditionExpression conditionexpressionOfCompatibility = compatibilityRule.getConditionExpression();
					if (conditionexpressionOfCompatibility != null) {
						createConditionExpressionNode(compatibilityRuleNode, conditionexpressionOfCompatibility);
					}
					List<ProductOfferingSpecification> compatibleOfferings = compatibilityRule.getCompatibleOfferings();
					if (!compatibleOfferings.isEmpty()) {
						createcompatibleOfferingsNode(compatibilityRuleNode, compatibleOfferings, pathDataList);
					}
					List<ProductOfferingSpecification> inCompatibleOfferings = compatibilityRule
							.getIncompatibleOfferings();
					if (!inCompatibleOfferings.isEmpty()) {
						createcompatibleOfferingsNode(compatibilityRuleNode, inCompatibleOfferings, pathDataList);
					}
				}
			}
			
			List<AvailabilityRule> availabilityRuleList = productOfferSpec.getAvailabilityRule();
			if (!availabilityRuleList.isEmpty()) {
				for (AvailabilityRule availabilityRule :availabilityRuleList) {
					Node availabilityRuleNode = productOfferNode.addNode(availabilityRule.getName(),
							RebusUtils.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(availabilityRuleNode, availabilityRule);
					/*
					 * ConditionExpression Node as child node of
					 * availabilityRuleNode
					 */
					ConditionExpression conditionexpressionOfAvailability = availabilityRule.getConditionExpression();
					if (conditionexpressionOfAvailability != null) {
						createConditionExpressionNode(availabilityRuleNode, conditionexpressionOfAvailability);
						;
					}

					List<Location> locaionList = availabilityRule.getLocation();
					if (!locaionList.isEmpty()) {
						for (Location listOfLocations : locaionList) {
							Node locationListNode = availabilityRuleNode.addNode(listOfLocations.getName(),
									RebusUtils.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(locationListNode, listOfLocations);
							if (listOfLocations.getAddress() != null) {
								Node addressNode = locationListNode.addNode(RebusUtils.ADDRESS);
								//31/05
								String apartmentNumber = listOfLocations.getAddress().getApartmentNumber();
								if(apartmentNumber!=null)
									addressNode.setProperty(RebusUtils.APARTMENT_NUMBER,apartmentNumber);
								
								addressNode.setProperty(RebusUtils.CITY, listOfLocations.getAddress().getCity());
								
								String country = listOfLocations.getAddress().getCountry();
								if(country!=null)
									addressNode.setProperty(RebusUtils.COUNTRY,country);
								
								String locality = listOfLocations.getAddress().getLocality();
								if(locality!=null)
									addressNode.setProperty(RebusUtils.LOCALITY,locality);
								
								AddressLocalization addressLocalization= listOfLocations.getAddress().getLocalization() ;
								if (addressLocalization != null) {
									Node localizationNode = addressNode.addNode(RebusUtils.LOCALIZATION);
									List<Localization> localizedCityList=addressLocalization.getLocalizedCityName();
									if(!localizedCityList.isEmpty())
									{
										for (Localization localization : localizedCityList) {
										Node localizedCityNode = localizationNode.addNode(RebusUtils.LOCALIZED_CITY);
										localizedCityNode.setProperty(RebusUtils.ID, localization.getId());
										localizedCityNode.setProperty(RebusUtils.LOCALE, localization.getLocale());
										localizedCityNode.setProperty(RebusUtils.LOCALIZED_VALUE,
												localization.getLocalizedValue());
									}
									}
									List<Localization> localizedStateOrProvinceList = addressLocalization.getLocalizedStateOrProvinceName();
									if(!localizedStateOrProvinceList.isEmpty()){
										for (Localization localization : localizedStateOrProvinceList) {
										Node localizedStateOrProvinceNode = localizationNode
												.addNode(RebusUtils.LOCALIZED_STATE_OR_PROVINCE);
										localizedStateOrProvinceNode.setProperty(RebusUtils.ID, localization.getId());
										localizedStateOrProvinceNode.setProperty(RebusUtils.LOCALE,
												localization.getLocale());
										localizedStateOrProvinceNode.setProperty(RebusUtils.LOCALIZED_VALUE,
												localization.getLocalizedValue());
									}
									}
									List<Localization> localizedStreetList = listOfLocations.getAddress().getLocalization().getLocalizedStreetName();
									if(!localizedStreetList.isEmpty()){
									for (Localization localization : localizedStreetList) {
										Node localizedStreetNode = localizationNode
												.addNode(RebusUtils.LOCALIZED_STREET);
										localizedStreetNode.setProperty(RebusUtils.ID, localization.getId());
										localizedStreetNode.setProperty(RebusUtils.LOCALE, localization.getLocale());
										localizedStreetNode.setProperty(RebusUtils.LOCALIZED_VALUE,
												localization.getLocalizedValue());
									}
									}
								}
								addressNode.setProperty(RebusUtils.POST_CODE,listOfLocations.getAddress().getPostCode());
										
								String postalAddressCode = listOfLocations.getAddress().getPostalAddressCode();
								if(postalAddressCode!=null)
									addressNode.setProperty(RebusUtils.POSTAL_ADDRESS_CODE,postalAddressCode);
								
								String stateOrProvince = listOfLocations.getAddress().getStateOrProvince();		
								if(stateOrProvince!=null)
									addressNode.setProperty(RebusUtils.STATE_OR_PROVINCE,stateOrProvince);
								
								String streetName = listOfLocations.getAddress().getStreetName();		
								if(streetName!=null)
									addressNode.setProperty(RebusUtils.STREET_NAME,listOfLocations.getAddress().getStreetName());
								
								String streetNumberFirst = listOfLocations.getAddress().getStreetNumberFirst();
								if(streetNumberFirst!=null)		
									addressNode.setProperty(RebusUtils.STREET_NUMBER_FIRST,listOfLocations.getAddress().getStreetNumberFirst());
										
								String streetNumberLast = listOfLocations.getAddress().getStreetNumberLast();
								if(streetNumberLast!=null)
									addressNode.setProperty(RebusUtils.STREET_NUMBER_LAST,listOfLocations.getAddress().getStreetNumberLast());
										
							}
							SupplierPartner supplierPartner = listOfLocations.getSupplierPartner();
							if (supplierPartner != null) {
								Node supplierPartnerNode = locationListNode.addNode(supplierPartner.getName(),
										RebusUtils.NT_UNSTRUCTURED);
								supplierPartnerNode.setProperty(RebusUtils.ID, supplierPartner.getId());
								supplierPartnerNode.setProperty(RebusUtils.NAME, supplierPartner.getName());
								supplierPartnerNode.setProperty(RebusUtils.SUPPLIER_ID,
										supplierPartner.getSupplierId());
								supplierPartnerNode.setProperty(RebusUtils.TAX_NUMBER, supplierPartner.getTaxNumber());
								supplierPartnerNode.setProperty(RebusUtils.VAT_ID, supplierPartner.getVatId());
							}
						}
					}

				}

			}
			createEligibilityRuleNode(productOfferNode, productOfferSpec);
			addSpecification(productOfferNode, productOfferSpec.getPricePlanSpecification());
			CustomerAccountType customerAccountType = productOfferSpec.getAccountType();
			if (customerAccountType != null) {
				Node customerAccountTypeNode = productOfferNode.addNode(productOfferSpec.getAccountType().getName(),
						RebusUtils.NT_UNSTRUCTURED);
				entitySpecificationSetProperty(customerAccountTypeNode, customerAccountType);
			}
			ProductSpecification productSpecification = productOfferSpec.getProductSpecification();
			if (productSpecification != null) {
				Node productSpecificationNode = productOfferNode.addNode(productSpecification.getName(),
						RebusUtils.NT_UNSTRUCTURED);
				entitySpecificationSetProperty(productSpecificationNode, productSpecification);
				specificationSetProperty(productSpecificationNode, productSpecification);
				
				List<PricePlanSpecification> pricePlanSpecificationList = productSpecification
						.getPricePlanSpecification();
				if (!pricePlanSpecificationList.isEmpty()) {
					addSpecification(productSpecificationNode, pricePlanSpecificationList);
				}
			

				
			}
			List<RelationshipSpecification> referencedSpecList = productOfferSpec.getReferencedSpecification();
			if (!referencedSpecList.isEmpty()) {
				Node referencedSpecsNode = productOfferNode.addNode(RebusUtils.REFERENCED_SPECIFICATIONS,
						RebusUtils.NT_UNSTRUCTURED);
				for (RelationshipSpecification referencedSpec : referencedSpecList) {
					Node referencedSpecificationNode = referencedSpecsNode.addNode(referencedSpec.getName());
					entitySpecificationSetProperty(referencedSpecificationNode, referencedSpec);
					List<Specification> relatedSpecList = referencedSpec.getRelatedSpecification();
					Node relatedSpecsNode = referencedSpecificationNode.addNode(RebusUtils.RELATED_SPECIFICATIONS,
							RebusUtils.NT_UNSTRUCTURED);
					for (Specification relatedSec : relatedSpecList) {
						PathModel pathModel = new PathModel();
						Node relatedSpecificationNode = relatedSpecsNode.addNode(relatedSec.getName());
						pathModel.setAbsolutePath(relatedSpecificationNode.getPath());
						pathModel.setName(relatedSec.getName());
						pathModel.setPathNode(relatedSpecificationNode);
						pathDataList.add(pathModel);
						entitySpecificationSetProperty(relatedSpecificationNode, relatedSec);
						//7/6
						relatedSpecificationNode.setProperty(RebusUtils.TYPE, RebusUtils.SUB_PRODUCT_OFFERING);
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the Category And its call subCategory() method
	 * if we have sub category in the current iterating object .
	 * 
	 * Passing catalogNode ,categoryList ,productOfferSpec
	 * ,referencedSpecPathMap ,pathDataList
	 * 
	 * @param catalogNode
	 *            is used to set the property .
	 * @param categoryList
	 *            is used to iterate the List of Category
	 * @param productOfferSpec
	 *            used to hold the product offer data
	 * @param pathDataList
	 *            is used to hold the exact location of reference specification
	 *            node where we need to set the absolute path of reference
	 *            specification .
	 * @param referencedSpecPathMap
	 *            used to hold the reference specification node absolute path.
	 * 
	 * @return void
	 * 
	 */
	private void writeCategory(Node catalogNode, List<Category> categoryList,
			ProductOfferingSpecification productOfferSpec, Map<String, String> referencedSpecPathMap,
			List<PathModel> pathDataList) {
		Node categoryNode = null;
		for (Category category : categoryList) {
			try {
				if (!catalogNode.hasNode(category.getName())) {
					categoryNode = catalogNode.addNode(category.getName(), RebusUtils.SLING_FOLDER);
					entitySpecificationSetProperty(categoryNode, category);
				} else {
					categoryNode = catalogNode.getNode(category.getName());
				}
				if (!category.getSubcategory().isEmpty()) {
					categoryNode = writeSubCategory(categoryNode, category.getSubcategory());
				}
				createProductOfferNode(categoryNode, productOfferSpec, referencedSpecPathMap, pathDataList);
			}

			catch (ValueFormatException valueEx) {
				log.info("Error:" + valueEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

			} catch (LockException lockEx) {

				log.info("Error:" + lockEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

			} catch (RepositoryException repositoryEx) {
				log.info("Error:" + repositoryEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

			} catch (Exception ex) {

				log.info("Error:" + ex.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

			}

		}
	}

	/**
	 * This method is used to recursive call to handle the scenario of Category
	 * inside subCategory
	 * 
	 * Passing adobe ,categoryList
	 * 
	 * @param adobe
	 *            is used to set the property .
	 * @param categoryList
	 *            is used to iterate the List of Category
	 * 
	 * @return void
	 * 
	 */

	private Node writeSubCategory(Node adobe, List<Category> categoryList) {
		Node subcategoryNode = null;
		for (Category subCategory : categoryList) {
			try {
				if (!adobe.hasNode(subCategory.getName())) {
					subcategoryNode = adobe.addNode(subCategory.getName(), RebusUtils.SLING_FOLDER);
					entitySpecificationSetProperty(subcategoryNode, subCategory);
				} else {
					subcategoryNode = adobe.getNode(subCategory.getName());
				}
				if (!subCategory.getSubcategory().isEmpty()) {
					subcategoryNode = writeSubCategory(subcategoryNode, subCategory.getSubcategory());
				}
			}

			catch (ValueFormatException valueEx) {
				log.info("Error:" + valueEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

			} catch (LockException lockEx) {

				log.info("Error:" + lockEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

			} catch (RepositoryException repositoryEx) {
				log.info("Error:" + repositoryEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

			} catch (Exception ex) {

				log.info("Error:" + ex.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

			}

		}
		return subcategoryNode;
	}

	/**
	 * This method is used for Recursive call to handle the compatibleOfferings
	 * 
	 * Passing compatibilityRuleNode ,compatibleOfferings ,pathDataList
	 * 
	 * @param compatibilityRuleNode
	 *            is used to set the property .
	 * @param compatibleOfferings
	 *            is used to iterate the List of product offering specification
	 * @param pathDataList
	 *            is used to hold the exact location of reference specification
	 *            node where we need to set the absolute path of reference
	 *            specification
	 * 
	 * @return void
	 * 
	 */
	private void createcompatibleOfferingsNode(Node compatibilityRuleNode,
			List<ProductOfferingSpecification> compatibleOfferings, List<PathModel> pathDataList) {
		try {
			Node compatibleOfferingsNode = null;
			for (ProductOfferingSpecification compatibleOfferingsObj : compatibleOfferings) {
				compatibleOfferingsNode = compatibilityRuleNode.addNode(compatibleOfferingsObj.getName());
				if (compatibleOfferingsObj != null) {
					productOfferingSpec(compatibleOfferingsObj, compatibleOfferingsNode, pathDataList);
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

}
