package uk.co.three.rebus.three_rebus.core.commons;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * This class is responsible for handling all the error Messages
 *  
 *   author
 *
 */
public class FetchErrorMessage {

	private static final Logger LOGGER = LoggerFactory.getLogger(FetchErrorMessage.class);

	public static String getErrorMessage(String errorMessage) {
		String message = "";
		try {
			ResourceBundle errorMessageBundle = ResourceBundle.getBundle("ErrorMessages", Locale.getDefault());
			message = errorMessageBundle.getString(errorMessage);
		} catch (Exception e) {
			LOGGER.info("Failed to fetch error message");
		}
		return message;
	}

}
