package uk.co.three.rebus.three_rebus.core.ui.sightly;

import java.util.List;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Reference;

import com.adobe.cq.sightly.WCMUse;
import com.adobe.cq.sightly.WCMUsePojo;

import epc.h3g.com.ProductOfferingSpecification;
import uk.co.three.rebus.three_rebus.core.models.ProductOfferingNodeData;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.service.SessionHandlerService;

public class ReadRepositoryData extends WCMUsePojo {

	public String detail;
	
	
	@Override
	public void activate() throws Exception {
		// TODO Auto-generated method stub

QueryProvider queryProviderImp = getSlingScriptHelper().getService(QueryProvider.class);
		
SessionHandlerService sessionHandlerService=getSlingScriptHelper().getService(SessionHandlerService.class);


	// we can populate JAX B Object values 
	//ProductOfferingSpecification productOfferingSpecification=queryProviderImp.getProductOfferSample(sessionHandlerService.openSession(), "Three Essential Plan (4GB data, 600 minutes)");
		//ProductOfferingSpecification productOfferingSpecification=queryProviderImp.getProductOfferDetails2(sessionHandlerService.openSession(), "Three Essential Plan (4GB data, 600 minutes)");
	List<ProductOfferingSpecification> productOfferingSpecification=queryProviderImp.getProductOfferSample(getResourceResolver().adaptTo(Session.class));	
	detail=productOfferingSpecification.get(0).getCatalogId();
	getResourceResolver().adaptTo(Session.class).logout();
	/*for(ProductOfferingSpecification pos:productOfferingSpecification)
		{
			detail = pos.getCatalogId();
		}
	*/
	}

	public String getDetails() {
		return this.detail;
	}
	
	
}
