package uk.co.three.rebus.three_rebus.core.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import uk.co.three.rebus.three_rebus.core.service.SessionHandlerService;

@Component(immediate = true)
@Service(value = { SessionHandlerService.class })

/**
 * This class is responsible for handling the session
 *  
 * @author 
 *
 */

public class SessionHandlerImpl implements SessionHandlerService {

	@Reference
	private ResourceResolverFactory resourceFactory;

	
	/* * This method is used to provide the ResourceResolver objects to perform operations.
	 * 
	 * @return ResourceResolver
	 * @exception LoginException
	 * 
	 */
	@Override
	public ResourceResolver getResourceResolver() throws LoginException {
		Map<String, Object> productImp = new HashMap<String, Object>();

		productImp.put(ResourceResolverFactory.SUBSERVICE, "readService");

		ResourceResolver resourceResolver = resourceFactory.getServiceResourceResolver(productImp);

		return resourceResolver;
	}

	/**
	 * This method is used to provide the session objects to perform Transactions.
	 * 
	 * @return session
	 * @exception LoginException
	 * 
	 */
	
	@Override
	public Session openSession() throws LoginException {
		Session session = null;
		ResourceResolver resourceResolver = getResourceResolver();
		session = resourceResolver.adaptTo(Session.class);

		return session;
	}

}
