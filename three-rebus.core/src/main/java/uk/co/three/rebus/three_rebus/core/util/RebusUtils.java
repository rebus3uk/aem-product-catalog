package uk.co.three.rebus.three_rebus.core.util;

import javax.jcr.Value;

public class RebusUtils {

	public static final String NAME = "name";
	public static final String TYPE = "nodeType";
	public static final String CATALOG_ID = "catalogId";
	public static final String DESCRIPTION = "description";
	public static final String PRODUCT_OFFERINGS_STATUS = "productofferingstatus";
	public static final String ID = "id";
	public static final String IS_ACTIVATED = "isActivated";

	public static final String IS_ADDON = "isAddon";
	public static final String IS_COMMERICAL = "isCommercial";
	public static final String IS_SHAREABLE = "isShareable";
	public static final String VALID_FROM = "validFrom";
	public static final String STATUS = "status";
	public static final String STORE_INVENTORY = "storeInventory";
	public static final String MAX_MULTIPICITY = "maxMultiplicity";
	public static final String MIN_MULTIPICITY = "minMultiplicity";
	public static final String IS_USABLE_FOR_CONTRACT_RENEW = "isUsableForContractRenew";
	public static final String IS_APPLICABILITY_EDITABLE = "isApplicabilityEditable";
	public static final String IS_OVERUSE_ALLOWED = "isOveruseAllowed";
	public static final String ROAMING_OR_INTERNATION = "roamingOrInternational";
	public static final String PRODUCT_SPECIFICATION_STATUS = "productspecificationstatus";
	public static final String HAS_STORE_INVENTORY = "hasStoreInventory";
	public static final String READABLE_VERSION = "readableVersion";
	public static final String IS_EDITABLE = "isEditable";
	public static final String IS_PRINTABLE = "isPrintable";

	public static final String IS_VISIBLE = "isVisible";
	public static final String MAX_SIZE = "maxSize";
	public static final String SCALE = "scale";

	public static final String ROAMING_OR_INTERNATIONAL = "roamingOrInternational";

	public static final String NT_UNSTRUCTURED = "nt:unstructured";

	public static final String SLING_FOLDER = "sling:Folder";

	public static final String AMOUNT = "amount";
	public static final String CURRENCY = "currency";

	public static final String IS_TOTAL_ALLOWANCE_EDITABLE = "isTotalAllowanceEditable";

	public static final String TOTAL_SPENDING_UNITS = "totalSpendingUnits";

	public static final String IS_MANDATORY = "isMandatory";

	public static final String IS_COMPLEX = "isComplex";
	public static final String SUPPLIER_ID = "supplierId";
	public static final String TAX_NUMBER = "taxNumber";
	public static final String VAT_ID = "vatId";
	public static final String START_OFFSET_UNITS = "startOffsetUnits";
	public static final String END_OFFSET_UNITS = "endOffsetUnits";
	public static final String VALUE = "value";
	public static final String EXTERNAL_ID = "externalId";
	public static final String REF_ID = "refId";
	public static final String VALID_TO = "validTo";
	public static final String APARTMENT_NUMBER = null;

	public static final String CITY = "city";
	public static final String COUNTRY = "country";
	public static final String LOCALITY = "locality";
	public static final String POST_CODE = "postCode";
	public static final String POSTAL_ADDRESS_CODE = "postalAddressCode";
	public static final String STATE_OR_PROVINCE = "stateOrProvince";
	public static final String STREET_NAME = "streetName";
	public static final String STREET_NUMBER_FIRST = "streetNumberFirst";
	public static final String STREET_NUMBER_LAST = "streetNumberLast";
	public static final String LOCALE = "locale";
	public static final String LOCALIZED_VALUE = "localizedValue";
	public static final String PHONE_NUMBER = "phoneNumber";

	public static final String VERSION = "version";

	public static final String PRODUCT_REFERENCE_PATH = "productRefPath";

	public static final String OUTSIDE_NETWORK = "isOutsideNetwork";
	public static final String REASON = "reason";
	public static final String TIME_STAMP = "timestamp";
	public static final String USER = "user";
	public static final String NEW_STATUS = "newStatus";
	public static final String OLD_STATUS = "oldStatus";
	public static final String NODE_MULTIPLICITY = "nodeMultiplicity";
	public static final String PATH = "path";
	public static final String KEY = "key";
	public static final String MIME_TYPE = "mimeType";
	public static final String CREDIT_AMOUNT = "creditAccount";
	public static final String DEBIT_AMOUNT = "debitAccount";
	public static final String AMOUNT_PERCENTAGE = "amountPercentage";
	public static final String REVENUE_RECOGNITION_TYPE = "RevenueRecongitionType";
	public static final String SEQUENCE = "sequence";
	public static final String FROM_24_HOUR = "from24Hour";
	public static final String TO_24_HOUR = "to24Hour";
	public static final String DAY = "day";
	public static final String VALUE_TYPE = "valueType";

	public static final String STATUS_HISTORY = "StatusHistory";
	public static final String LOCALIZED_DESCRIPTION = "LocalizedDescription";
	public static final String LOCALIZED_NAME = "LocalizedName";
	public static final String CONDITION_EXPRESSION_ITEM = "conditionExpressionItem";
	public static final String PERIODICITY_UNITS = "periodicityUnits";
	public static final String TOTAL_CYCLES = "totalCycles";
	public static final String RECURRING = "recurring";
	public static final String PERIODICITY = "perioditicy";
	public static final String ALERT = "alert";
	public static final String FLAT_RATE = "flatRate";
	public static final String SPENDING_UNITS_FOR_PRICE = "spendingUnitsForPrice";
	public static final String IS_AMOUNT_EDITABLE = "amountEditable";

	public static final String NEWSTATUS = "newStatus";
	public static final String OLDSTATUS = "oldStatus";

	public static final String IS_INVOICE = "invoice";
	public static final String IS_LIMIT_EDITABLE = "limitEditable";
	public static final String DISCOUNT = "discount";

	public static final String SPENDING_LIMIT = "Spending Limit";
	public static final String BILLING_CYCLE = "Billing Cycle";
	public static final String SPENDING_LIMIT_RULE = "Spending Limit Rule";
	public static final String LIMIT = "Limit";
	public static final String AMOUNT_NODE = "Amount";
	public static final String COMMISSION_EVENT = "commissionEvent";
	public static final String COMMISSION_TYPE = "commissionType";

	public static final String CHARACTERISTIC_VALUE = "Characteristic Value";
	public static final String ADDRESS = "Address";
	public static final String LOCALIZATION = "Localization";
	public static final String LOCALIZED_CITY = "Localized City";
	public static final String LOCALIZED_STATE_OR_PROVINCE = "Localized State Or Province";
	public static final String LOCALIZED_STREET = "Localized Street";
	public static final String REFERENCED_SPECIFICATIONS = "Referenced Specifications";
	public static final String RELATED_SPECIFICATIONS = "Related Specifications";
	public static final String ALLOWED_VALUE = "Allowed Value";
	public static final String DEFAULT_VALUE = "Default Value";
	public static final String MAX_VALUE = "Max Value";
	public static final String MIN_VALUE = "Min Value";
	public static final String VALUE_TYPE_NODE = "ValueType";
	public static final String PATH_SPECIFICATION = "Path Specification";
	public static final String CONTENT_RESOURCE = "Content Resource";
	public static final String SINGLE_VIEW_DETAILS = "SingleView Details";
	public static final String FIXED_AMOUNT = "Fixed Amount";
	public static final String OFFSET_AMOUNT = "Offset Amount";
	public static final String VALUE_EXPRESSION_ITEM = "Value Expression Item";
	public static final String PERIODICITY_NODE = "Periodicity";
	public static final String ALERT_NODE = "Alert";
	public static final String REFERENCED_ALLOWANCE_GROUP = "Referenced Allowance Group";
	public static final String PRICE = "Price";
	public static final String ROLL_OVER = "Roll Over";
	public static final String TOTAL_FINANCIAL = "Total Financial";
	public static final String APPLICABILITY_SPECIFICATION = "Applicability Specification";
	public static final String TIME_PERIOD = "Time Period";
	public static final String DAY_NODE = "Day";
	public static final String PHONE_NUMBER_NODE = "Phone Number";
	public static final String STORE = "store";
	public static final String SUB_PRODUCT_OFFERING = "SUB_PRODUCT_OFFERING";
	public static final String PRODUCT_OFFERING = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/three_rebus/products/catalogOne]) and CONTAINS(s.*, '*')  AND nodeType='PRODUCT_OFFERING'";

}
