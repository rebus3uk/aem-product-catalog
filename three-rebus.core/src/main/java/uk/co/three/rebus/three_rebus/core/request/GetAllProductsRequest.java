/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.request;

import javax.jcr.Session;

/**
 * This is the request class for getting all the products
 * This class does not have any data members at this point
 *  
 * @author DD00493288
 *
 */
public class GetAllProductsRequest extends BaseRequest {

	private Session session = null;

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}
	
}
