package uk.co.three.rebus.three_rebus.core.commons;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

public class ErrorCodeConstants {
	// this will call enum constructor with one String argument
	public static final int NODE_DOES_NOT_EXIST = 101;

	public static final String NODE_DOES_NOT_EXIST_MESSAGE = "error.node.doesnot.exist";

	public static final int REPOSITORY_FAILURE = 102;

	public static final String REPOSITORY_FAILURE_MESSAGE = "error.repository.failure.exist";

	public static final int PATHNOTFOUND_FAILURE = 103;

	public static final String PATHNOTFOUND_FAILURE_MESSAGE = "error.path.not.found";

	public static final int PRODUCT_OFFER_LIST_EMPTY = 104;

	public static final String PRODUCT_OFFER_LIST_EMPTY_MESSAGE = "error.product.offer.empty";

	public static final int PRODUCT_OFFER_EMPTY = 105;

	public static final String PRODUCT_OFFER_EMPTY_MESSAGE = "error.product.offer.empty";

	public static final int FILE_NOT_FOUND = 106;

	public static final String FILE_NOT_FOUND_MESSAGE = "error.file.not.found";

	public static final int SYSTEM_UNAVAILABLE = 107;

	public static final String SYSTEM_UNAVAILABLE_MESSAGE = "error.system.unavailable";

	public static final Integer NODE_EXCEPTION = 108;

	public static final String NODE_EXCEPTION_MESSAGE = "error.node.exeption";

	public static final Integer LOGIN_EXEPTION = 109;

	public static final String LOGIN_EXEPTION_MESSAGE = "error.login.exception";

	public static final Integer VALUE_FORMATE_EXEPTION = 110;

	public static final String VALUE_FORMATE_MESSAGE = "error.value.format.exception";

	public static final Integer VERSION_EXEPTION = 111;

	public static final String VERSION_EXEPTION_MESSAGE = "error.version.exception";

	public static final Integer LOCK_EXEPTION = 112;

	public static final String LOCK_EXEPTION_MESSAGE = "error.lock.exception";

	public static final Integer CONSTRAIN_VIOLATION_EXEPTION = 113;

	public static final String CONSTRAIN_VIOLATION_EXEPTION_MESSAGE = "error.constraint.violation.exception";

}
