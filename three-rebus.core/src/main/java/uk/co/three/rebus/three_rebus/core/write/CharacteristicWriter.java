package uk.co.three.rebus.three_rebus.core.write;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.ApplicabilityRule;
import epc.h3g.com.ApplicabilitySpecification;
import epc.h3g.com.CharacteristicGroupSpecification;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CharacteristicValue;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.ConditionExpressionItem;
import epc.h3g.com.Country;
import epc.h3g.com.EntityContentResource;
import epc.h3g.com.EntitySpecification;
import epc.h3g.com.Localization;
import epc.h3g.com.Location;
import epc.h3g.com.Money;
import epc.h3g.com.Specification;
import epc.h3g.com.StatusHistory;
import epc.h3g.com.TimePeriod;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

/**
 * This class is responsible for handling Characteristic data And inserting the
 * data into repository . Its super class of SpecificationWriter.
 * 
 * @author MS00506946
 *
 */

public class CharacteristicWriter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	public Map<Integer, String> errorMap = new HashMap<Integer, String>();

	/**
	 * This method is used to add the nodes and properties which are related to
	 * characteristicSpecification
	 * 
	 * Passing productOfferNode ,listCharSpec ,characteristicSpecification
	 * 
	 * @param productOfferNode
	 *            is used to get the ProductOfferingSpecification object to
	 *            productOfferNode object .
	 * @param listCharSpec
	 *            is used to iterate the List of CharacteristicSpecification
	 * @param characteristicSpecification
	 *            used to hold the characteristicSpecification data .
	 * 
	 * @return void
	 * 
	 */

	protected void characteristicSpecification(Node productOfferNode, CharacteristicSpecification CharSpec) {
		

		try {
			Node characteristicSpecificationNode = productOfferNode.addNode(CharSpec.getName(), RebusUtils.NT_UNSTRUCTURED);

			entitySpecificationSetProperty(characteristicSpecificationNode, CharSpec);

			/* Mandatory Properties */
			characteristicSpecificationNode.setProperty(RebusUtils.IS_COMMERICAL,
					CharSpec.isCommercial());
			characteristicSpecificationNode.setProperty(RebusUtils.IS_EDITABLE,
					CharSpec.isEditable());
			characteristicSpecificationNode.setProperty(RebusUtils.IS_MANDATORY,
					CharSpec.isMandatory());
			characteristicSpecificationNode.setProperty(RebusUtils.IS_PRINTABLE,
					CharSpec.isPrintable());
			characteristicSpecificationNode.setProperty(RebusUtils.IS_VISIBLE,
					CharSpec.isVisible());
			characteristicSpecificationNode.setProperty(RebusUtils.MIN_MULTIPICITY,
					CharSpec.getMinMultiplicity().longValue());

			/* BigInteger Properties */
			//31/5
			if(CharSpec.getMaxMultiplicity() != null)
			{
				characteristicSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,CharSpec.getMaxMultiplicity().longValue());
			}
			if(CharSpec.getMaxSize() != null)
			{
				characteristicSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,CharSpec.getMaxSize().longValue());
			}
			if(CharSpec.getScale() != null)
			{
				characteristicSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,CharSpec.getScale().longValue());
			}
			List<CharacteristicValue> allowedValues = CharSpec.getAllowedValue();
			if (!allowedValues.isEmpty()) {
				for (CharacteristicValue allowedValue : allowedValues) {
					createCharacteristicValueNode(characteristicSpecificationNode, allowedValue,
							RebusUtils.ALLOWED_VALUE);

				}
			}
			CharacteristicValue defaultValue = CharSpec.getDefaultValue();
			if (defaultValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, defaultValue,
						RebusUtils.DEFAULT_VALUE);

			}
			CharacteristicValue maxValue = CharSpec.getMaxValue();
			if (maxValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, maxValue,
						RebusUtils.MAX_VALUE);

			}
			CharacteristicValue minValue = CharSpec.getMinValue();
			if (minValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, minValue,
						RebusUtils.MIN_VALUE);
			}
			if (CharSpec.getValueType() != null) {
				Node valueTypeNode = characteristicSpecificationNode.addNode(RebusUtils.VALUE_TYPE_NODE);
				valueTypeNode.setProperty(RebusUtils.VALUE_TYPE,
						CharSpec.getValueType().value());
			}

		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	

}

	/**
	 * This method is used for Recursive call to handle the
	 * characteristicGroupSpecification inside the characteristicSpecification
	 * 
	 * Passing node ,specification
	 * 
	 * @param node
	 *            is used to set the property .
	 * @param specification
	 *            is used to get the CharacteristicGroupSpecification object
	 * 
	 * @return void
	 * 
	 */

	protected void createCharacteristicGroupSpecificationNode(Node node, Specification specification) {
		try {
			List<CharacteristicGroupSpecification> characteristicGroupSpecification = specification
					.getCharacteristicGroupSpecification();
			for (CharacteristicGroupSpecification characteristicGroupSpecificationobj : characteristicGroupSpecification) {
				Node characteristicGroupSpecificationNode = node.addNode(characteristicGroupSpecificationobj.getName());
				characteristicGroupSpecificationNode.setProperty(RebusUtils.MIN_MULTIPICITY,
						characteristicGroupSpecificationobj.getMinMultiplicity().longValue());
				characteristicGroupSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,
						characteristicGroupSpecificationobj != null
								? characteristicGroupSpecificationobj.getMaxMultiplicity().longValue() : 0);
				List<CharacteristicSpecification> characteristicSpecifications = characteristicGroupSpecificationobj.getCharacteristicSpecifications();
				if(!characteristicSpecifications.isEmpty()){
					for(CharacteristicSpecification charSpec:characteristicSpecifications)
					{
						characteristicSpecification(node,charSpec);
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used for Recursive call to handle the characteristicValue
	 * inside the characteristicSpecification
	 * 
	 * Passing characteristicSpecificationNode ,characteristicValue ,nodeName
	 * 
	 * @param characteristicSpecificationNode
	 *            is used to set the property .
	 * @param nodeName
	 *            is used to pass the String value
	 * 
	 * @return void
	 * 
	 */

	private void createCharacteristicValueNode(Node characteristicSpecificationNode,
			CharacteristicValue characteristicValue, String nodeName) {
		try {
			Node characteristicValueNode = characteristicSpecificationNode.addNode(nodeName);
			characteristicValueNode.setProperty(RebusUtils.VALUE_TYPE, characteristicValue.getValueType().value());
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * entitySpecification
	 * 
	 * Passing entitySpecificationNode ,entitySpecification
	 * 
	 * @param entitySpecificationNode
	 *            is used to set the property
	 * @param entitySpecification
	 *            is used to hold the entity Specification Data
	 * 
	 * @return void
	 * 
	 */

	protected void entitySpecificationSetProperty(Node entitySpecificationNode,
			EntitySpecification entitySpecification) {
		try {
			entitySpecificationNode.setProperty(RebusUtils.ID, entitySpecification.getId());
			entitySpecificationNode.setProperty(RebusUtils.CATALOG_ID, entitySpecification.getCatalogId());
			entitySpecificationNode.setProperty(RebusUtils.NAME, entitySpecification.getName());
			entitySpecificationNode.setProperty(RebusUtils.TYPE, entitySpecification.getType());
			entitySpecificationNode.setProperty(RebusUtils.VALID_FROM,
					entitySpecification.getValidFrom().toGregorianCalendar());
			if (entitySpecification.getDescription() != null)
				entitySpecificationNode.setProperty(RebusUtils.DESCRIPTION, entitySpecification.getDescription());
			if (entitySpecification.getExternalId() != null)
				entitySpecificationNode.setProperty(RebusUtils.EXTERNAL_ID, entitySpecification.getExternalId());
			if (entitySpecification.getRefId() != null)
				entitySpecificationNode.setProperty(RebusUtils.REF_ID, entitySpecification.getRefId());
			if (entitySpecification.getValidTo() != null)
				entitySpecificationNode.setProperty(RebusUtils.VALID_TO,
						entitySpecification.getValidTo().toGregorianCalendar());
			if (entitySpecification.getVersion() != null) {
				Node specificationVersionNode = entitySpecificationNode
						.addNode(entitySpecification.getVersion().getName());
				specificationVersionNode.setProperty(RebusUtils.READABLE_VERSION,
						entitySpecification.getVersion().getReadableVersion());
				specificationVersionNode.setProperty(RebusUtils.STATUS,
						entitySpecification.getVersion().getStatus().value());
				if (!entitySpecification.getVersion().getHistory().isEmpty()) {
					for (StatusHistory statusHistory : entitySpecification.getVersion().getHistory()) {
						Node statusHistoryNode = specificationVersionNode.addNode("StatusHistory");
						statusHistoryNode.setProperty(RebusUtils.ID, statusHistory.getId());
						statusHistoryNode.setProperty(RebusUtils.REASON, statusHistory.getReason());
						statusHistoryNode.setProperty(RebusUtils.TIME_STAMP,
								statusHistory.getTimestamp().toGregorianCalendar());
						statusHistoryNode.setProperty(RebusUtils.USER, statusHistory.getUser());
						statusHistoryNode.setProperty(RebusUtils.NEW_STATUS, statusHistory.getNewStatus().value());
						statusHistoryNode.setProperty(RebusUtils.OLD_STATUS, statusHistory.getOldStatus().value());
					}
				}
			}
			if (entitySpecification.getPathSpecification() != null) {
				Node pathSpecificationNode = entitySpecificationNode.addNode(RebusUtils.PATH_SPECIFICATION);
				pathSpecificationNode.setProperty(RebusUtils.NODE_MULTIPLICITY,
						entitySpecification.getPathSpecification().getNodeMultiplicity());
				pathSpecificationNode.setProperty(RebusUtils.PATH,
						entitySpecification.getPathSpecification().getPath());
			}
			if (!entitySpecification.getContentResource().isEmpty()) {
				for (EntityContentResource entityContentResource : entitySpecification.getContentResource()) {
					Node contentResourceNode = entitySpecificationNode.addNode(RebusUtils.CONTENT_RESOURCE);
					contentResourceNode.setProperty(RebusUtils.ID, entityContentResource.getId());
					contentResourceNode.setProperty(RebusUtils.KEY, entityContentResource.getKey());
					contentResourceNode.setProperty(RebusUtils.MIME_TYPE, entityContentResource.getMimeType());
					contentResourceNode.setProperty(RebusUtils.TYPE, entityContentResource.getType().value());
					contentResourceNode.setProperty(RebusUtils.VALUE, entityContentResource.getValue().toString());
				}
			}
			if (!entitySpecification.getLocalizedDescription().isEmpty()) {
				for (Localization localizedDescription : entitySpecification.getLocalizedDescription()) {
					Node localizedDescriptionNode = entitySpecificationNode.addNode(RebusUtils.LOCALIZED_DESCRIPTION);
					localizedDescriptionNode.setProperty(RebusUtils.ID, localizedDescription.getId());
					localizedDescriptionNode.setProperty(RebusUtils.LOCALE, localizedDescription.getLocale());
					localizedDescriptionNode.setProperty(RebusUtils.LOCALIZED_VALUE,
							localizedDescription.getLocalizedValue());
				}
			}
			if (!entitySpecification.getLocalizedName().isEmpty()) {
				for (Localization localizedName : entitySpecification.getLocalizedName()) {
					Node localizedNameNode = entitySpecificationNode.addNode(RebusUtils.LOCALIZED_NAME);
					localizedNameNode.setProperty(RebusUtils.ID, localizedName.getId());
					localizedNameNode.setProperty(RebusUtils.LOCALE, localizedName.getLocale());
					localizedNameNode.setProperty(RebusUtils.LOCALIZED_VALUE, localizedName.getLocalizedValue());
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the AmountNode and set the related properties
	 * .
	 * 
	 * Passing itemNode ,money ,nodeName
	 * 
	 * @param itemNode
	 *            object used to set the property .
	 * @param money
	 *            object is used to .
	 * 
	 * @return void
	 * 
	 */

	protected void createAmountNode(Node itemNode, Money money, String nodeName) {
		try {
			Node priceNode = itemNode.addNode(nodeName);
			priceNode.setProperty(RebusUtils.CURRENCY, money.getCurrency());
			priceNode.setProperty(RebusUtils.AMOUNT, money.getAmount());
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the applicabilitySpecNode and set the related
	 * properties
	 * 
	 * Passing priceItemSpecificationNode ,applicabilitySpecification
	 * 
	 * @param priceItemSpecificationNode
	 *            .
	 * @param applicabilitySpecification
	 *            object is used to hold the applicabilitySpecification data .
	 * 
	 * @return void
	 * 
	 */

	protected void applicabilitySpecNode(Node priceItemSpecificationNode,
			ApplicabilitySpecification applicabilitySpecification) {
		try {
			Node applicabilityspecificationNode = priceItemSpecificationNode
					.addNode(RebusUtils.APPLICABILITY_SPECIFICATION);
			//31/05
			if(applicabilitySpecification.isApplicabilityEditable() != null)
			{
				applicabilityspecificationNode.setProperty(RebusUtils.IS_APPLICABILITY_EDITABLE,
						applicabilitySpecification.isApplicabilityEditable());
			}
			if(applicabilitySpecification.isOveruseAllowed() != null)
			{
				applicabilityspecificationNode.setProperty(RebusUtils.IS_OVERUSE_ALLOWED,
						applicabilitySpecification.isOveruseAllowed() ); 
			}
			

			ApplicabilityRule applicabilityRule = applicabilitySpecification.getRule();
			if (applicabilityRule != null) {
				applicabilityRuleNode(applicabilityspecificationNode, applicabilityRule);
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * applicabilityRule .
	 * 
	 * Passing node ,applicabilityRule
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param applicabilityRule
	 *            object is used to hold the applicabilityRule data .
	 * 
	 * @return void
	 * 
	 */

	protected void applicabilityRuleNode(Node node, ApplicabilityRule applicabilityRule) {
		try {
			Node applicabilityRuleNode = node.addNode(applicabilityRule.getName());
			entitySpecificationSetProperty(applicabilityRuleNode, applicabilityRule);
			if (applicabilityRule.isRoamingOrInternational() != null) {
				applicabilityRuleNode.setProperty(RebusUtils.ROAMING_OR_INTERNATIONAL,
						applicabilityRule.isRoamingOrInternational());
			}
			if (applicabilityRule.isOutsideNetwork() != null) {
				applicabilityRuleNode.setProperty(RebusUtils.OUTSIDE_NETWORK, applicabilityRule.isOutsideNetwork());
			}
			ConditionExpression conditionExpressionOfApplicability = applicabilityRule.getConditionExpression();
			if (conditionExpressionOfApplicability != null) {
				createConditionExpressionNode(applicabilityRuleNode, conditionExpressionOfApplicability);
			}
			List<Country> countryList = applicabilityRule.getCountry();
			if (!countryList.isEmpty()) {
				for (Country country : countryList) {
					Node countryNode = applicabilityRuleNode.addNode(country.getName(), RebusUtils.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(countryNode, country);
				}
			}
			List<Location> locationList = applicabilityRule.getLocation();
			if (!locationList.isEmpty()) {
				for (Location location : locationList) {
					Node locationNode = applicabilityRuleNode.addNode(location.getName());
					entitySpecificationSetProperty(locationNode, location);
				}
			}
			List<TimePeriod> timePeriodList = applicabilityRule.getTimePeriod();
			if (!timePeriodList.isEmpty()) {
				for (TimePeriod timePeriod : timePeriodList) {
					Node timePeriodNode = applicabilityRuleNode.addNode(RebusUtils.TIME_PERIOD);
					if (timePeriod.getDay().value() != null) {
						Node dayNode = timePeriodNode.addNode(RebusUtils.DAY_NODE);
						dayNode.setProperty(RebusUtils.DAY, timePeriod.getDay().value());
					}
					timePeriodNode.setProperty(RebusUtils.FROM_24_HOUR,
							timePeriod.getFrom24Hour() != null ? timePeriod.getFrom24Hour().longValue() : 0);
					timePeriodNode.setProperty(RebusUtils.TO_24_HOUR,
							timePeriod.getTo24Hour() != null ? timePeriod.getTo24Hour().longValue() : 0);
				}
			}
			List<String> phoneNumberList = applicabilityRule.getPhoneNumber();
			if (!phoneNumberList.isEmpty()) {
				for (String phoneNumber : phoneNumberList) {
					Node phoneNumberNode = applicabilityRuleNode.addNode(RebusUtils.PHONE_NUMBER_NODE);
					phoneNumberNode.setProperty(RebusUtils.PHONE_NUMBER, phoneNumber);
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the ConditionExpressionNode and set the
	 * properties .
	 * 
	 * Passing node ,condition expression
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param conditionexpression
	 *            object is used to hold the conditionexpression data .
	 * 
	 * @return void
	 * 
	 */

	protected void createConditionExpressionNode(Node node, ConditionExpression conditionexpression) {
		try {
			if (conditionexpression != null) {
				Node conditionExpressionNode = node.addNode(conditionexpression.getName());
				entitySpecificationSetProperty(conditionExpressionNode, conditionexpression);
				List<ConditionExpressionItem> conditionExpressionItemList = conditionexpression.getItem();
				if (!conditionExpressionItemList.isEmpty()) {
					for (ConditionExpressionItem conditionExpressionItem : conditionExpressionItemList) {
						Node conditionExpressionItemNode = conditionExpressionNode
								.addNode(RebusUtils.CONDITION_EXPRESSION_ITEM);
						conditionExpressionItemNode.setProperty(RebusUtils.SEQUENCE,
								conditionExpressionItem.getSequence().longValue());
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}
}
