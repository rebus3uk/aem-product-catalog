package uk.co.three.rebus.three_rebus.core.util;

import java.util.Map;
import java.util.Set;

import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;

/**
 * This class is responsible for handling the error objects.
 * @author MS00506946
 *
 */
public class CommonUtils {

	public static void setErrorMessages(ProductDataGetResponse productDataGetResponse, Map<Integer, String> errorMap) {
		Set<Integer> errorCodes = errorMap.keySet();
		if (errorCodes != null) {
			for (Integer code : errorCodes) {
				productDataGetResponse.addErrorObject(code, errorMap.get(code));
			}
		}
	}

}
