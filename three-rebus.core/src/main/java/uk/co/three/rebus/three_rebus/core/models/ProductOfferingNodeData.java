package uk.co.three.rebus.three_rebus.core.models;

import java.io.Serializable;
import java.util.GregorianCalendar;

public class ProductOfferingNodeData implements Serializable {

	private String catalogId;
	private String description;
	private String name;
	private Long id;
	private String type;
	private GregorianCalendar validFrom;
	private Boolean isActivated;
	private Boolean isAddon;
	private Boolean isCommercial;
	private Boolean isComplex;
	private Boolean isShareable;
	private String productofferingstatus;

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public GregorianCalendar getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(GregorianCalendar validFrom) {
		this.validFrom = validFrom;
	}

	public Boolean getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Boolean isActivated) {
		this.isActivated = isActivated;
	}

	public Boolean getIsAddon() {
		return isAddon;
	}

	public void setIsAddon(Boolean isAddon) {
		this.isAddon = isAddon;
	}

	public Boolean getIsCommercial() {
		return isCommercial;
	}

	public void setIsCommercial(Boolean isCommercial) {
		this.isCommercial = isCommercial;
	}

	public Boolean getIsComplex() {
		return isComplex;
	}

	public void setIsComplex(Boolean isComplex) {
		this.isComplex = isComplex;
	}

	public Boolean getIsShareable() {
		return isShareable;
	}

	public void setIsShareable(Boolean isShareable) {
		this.isShareable = isShareable;
	}

	public String getProductofferingstatus() {
		return productofferingstatus;
	}

	public void setProductofferingstatus(String productofferingstatus) {
		this.productofferingstatus = productofferingstatus;
	}

}
