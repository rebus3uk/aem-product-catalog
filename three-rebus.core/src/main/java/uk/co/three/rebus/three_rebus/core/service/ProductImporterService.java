package uk.co.three.rebus.three_rebus.core.service;

import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;

public interface ProductImporterService {
	public void doImport();
	public ProductDataGetResponse getProductDataResponse();

}
