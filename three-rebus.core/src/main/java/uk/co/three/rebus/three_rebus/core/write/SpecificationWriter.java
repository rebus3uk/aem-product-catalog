package uk.co.three.rebus.three_rebus.core.write;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.AccountingItemSpecification;
import epc.h3g.com.AccountingSchemeSpecification;
import epc.h3g.com.AllowanceGroupRelationshipSpecification;
import epc.h3g.com.AllowanceGroupSpecification;
import epc.h3g.com.AllowancePeriodicity;
import epc.h3g.com.AllowanceSpecification;
import epc.h3g.com.ApplicabilityRule;
import epc.h3g.com.ApplicabilitySpecification;
import epc.h3g.com.BillingCycleSpecification;
import epc.h3g.com.Characteristic;
import epc.h3g.com.CharacteristicGroupSpecification;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.ChargeSpecification;
import epc.h3g.com.ChargeType;
import epc.h3g.com.CommissionSpecification;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.CustomerSegment;
import epc.h3g.com.DiscountSpecification;
import epc.h3g.com.EligibilityRule;
import epc.h3g.com.MarketSegment;
import epc.h3g.com.Money;
import epc.h3g.com.PeriodRule;
import epc.h3g.com.PriceItemSpecification;
import epc.h3g.com.PricePlanSpecification;
import epc.h3g.com.ProductOfferingSpecification;
import epc.h3g.com.RelationshipSpecification;
import epc.h3g.com.RolloverSpecification;
import epc.h3g.com.SellingChannel;
import epc.h3g.com.Specification;
import epc.h3g.com.SpendingLimitRule;
import epc.h3g.com.SpendingLimitSpecification;
import epc.h3g.com.SpendingUnit;
import epc.h3g.com.StatusHistory;
import epc.h3g.com.ValueExpression;
import epc.h3g.com.ValueExpressionItem;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

/**
 * This class is responsible for handling Specification data And inserting the
 * data into repository . Its super class of CatalogInfomationWriter. And Sub
 * class of CharacteristicWriter
 * 
 * @author MS00506946
 *
 */
public class SpecificationWriter extends CharacteristicWriter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * This method is used to add the nodes and properties which are related to
	 * pricePlanSpecification
	 * 
	 * Passing productOfferNode ,pricePlanSpecificationList
	 * 
	 * @param productOfferNode
	 *            is used to get the PricePlanSpecification object to
	 *            productOfferNode object .
	 * @param pricePlanSpecificationList
	 *            is used to iterate the List of PricePlanSpecification
	 * 
	 * @return void
	 * 
	 */

	protected void addSpecification(Node productOfferNode, List<PricePlanSpecification> pricePlanSpecificationList) {
		pricePlanSpecificationList.stream().filter(pricePlanSpecification -> pricePlanSpecification != null)
				.forEach(pricePlanSpecification -> {
					try {
						Node pricePlanSpecificationNode = productOfferNode.addNode(pricePlanSpecification.getName(),
								RebusUtils.NT_UNSTRUCTURED);
						entitySpecificationSetProperty(pricePlanSpecificationNode, pricePlanSpecification);
						specificationSetProperty(pricePlanSpecificationNode, pricePlanSpecification);
						pricePlanSpecificationNode.setProperty(RebusUtils.STATUS,
								pricePlanSpecification.getStatus().name());
						pricePlanSpecificationNode.setProperty(RebusUtils.STORE_INVENTORY,
								pricePlanSpecification.isStoreInventory());
						List<PriceItemSpecification> priceItemSpecList = pricePlanSpecification.getItem();
						priceItemSpec(pricePlanSpecificationNode, priceItemSpecList);
						List<Characteristic> characteristicList = pricePlanSpecification.getCharacteristic();
						if (!characteristicList.isEmpty()) {
							characteristicList.forEach(characteristic -> {
								try {
									Node characteristicNode = pricePlanSpecificationNode
											.addNode(characteristic.getName(), RebusUtils.NT_UNSTRUCTURED);
									entitySpecificationSetProperty(characteristicNode, characteristic);
									CharacteristicSpecification characteristicSpecification = characteristic.getSpecification();
									if(characteristicSpecification!=null)
									{
										characteristicSpecification(characteristicNode,characteristicSpecification);		
									}
									
									
									
									if (characteristic.getValue() != null) {
										Node characteristicValueNode = characteristicNode
												.addNode(RebusUtils.CHARACTERISTIC_VALUE);
										characteristicValueNode.setProperty(RebusUtils.VALUE,
												characteristic.getValue().getValue().toString());
										characteristicValueNode.setProperty(RebusUtils.VALUE_TYPE,
												characteristic.getValue().getValueType().value());
									}
								} catch (Exception e) {
									log.info("Error:" + e.getLocalizedMessage());
									errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage
											.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
								}
							});
						}
						List<AllowanceGroupSpecification> allowanceGroupSpecification = pricePlanSpecification
								.getAllowanceGroup();
						if (!allowanceGroupSpecification.isEmpty()) {
							for (AllowanceGroupSpecification allowanceGroupSpecificationObj : allowanceGroupSpecification) {
								createAllowancegroupSpecNode(pricePlanSpecificationNode,
										allowanceGroupSpecificationObj);
							}
						}
						PeriodRule periodRule = pricePlanSpecification.getPeriodRule();
						if (periodRule != null) {
							Node periodRuleNode = pricePlanSpecificationNode.addNode(periodRule.getName(),
									RebusUtils.NT_UNSTRUCTURED);
							periodRuleNode.setProperty(RebusUtils.START_OFFSET_UNITS, periodRule.getStartOffsetUnits());
							if (periodRule.getEndOffsetUnits() != 0)
								periodRuleNode.setProperty(RebusUtils.END_OFFSET_UNITS, periodRule.getEndOffsetUnits());
							if (periodRule.getEndOffsetUnitType() != null) {
								periodRuleNode.setProperty(RebusUtils.VALUE, periodRule.getEndOffsetUnitType().value());
							}
							if (periodRule.getStartOffsetUnitType() != null) {
								periodRuleNode.setProperty(RebusUtils.VALUE,
										periodRule.getStartOffsetUnitType().value());
							}
							if (periodRule.getEndMark() != null) {
								Node endMarkNode = periodRuleNode.addNode(periodRule.getEndMark().getName(),
										RebusUtils.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(endMarkNode, periodRule.getEndMark());
							}
							if (periodRule.getStartMark() != null) {
								Node startMarkNode = periodRuleNode.addNode(periodRule.getStartMark().getName(),
										RebusUtils.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(startMarkNode, periodRule.getStartMark());
							}
						}
						createEligibilityRuleNode(pricePlanSpecificationNode, pricePlanSpecification);
					} catch (Exception ex) {
						log.info("Error:" + ex.getLocalizedMessage());
						errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
								FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
					}
				});
	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * Specification
	 * 
	 * Passing node ,specification
	 * 
	 * @param node
	 *            is used to set the property
	 * @param specification
	 *            is used to hold the Specification Data
	 * 
	 * @return void
	 * 
	 */

	protected void specificationSetProperty(Node node, Specification specification) {
		try {
			List<CharacteristicGroupSpecification> characteristicGroupSpecificationList = specification
					.getCharacteristicGroupSpecification();
			if (!characteristicGroupSpecificationList.isEmpty()) {
				for (CharacteristicGroupSpecification characteristicGroupSpecificationobj : characteristicGroupSpecificationList) {
					Node characteristicGroupSpecificationNode = node
							.addNode(characteristicGroupSpecificationobj.getName());
					entitySpecificationSetProperty(characteristicGroupSpecificationNode,
							characteristicGroupSpecificationobj);
					
					List<CharacteristicSpecification> charSpecList = specification.getCharacteristicSpecification();
					if(!charSpecList.isEmpty())
					{
						for(CharacteristicSpecification characteristicSpecification:charSpecList)
						{
							characteristicSpecification(node, characteristicSpecification);
						}
					}
					characteristicGroupSpecificationNode.setProperty(RebusUtils.MIN_MULTIPICITY,
							characteristicGroupSpecificationobj.getMinMultiplicity().longValue());
					characteristicGroupSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,
							characteristicGroupSpecificationobj != null
									? characteristicGroupSpecificationobj.getMaxMultiplicity().longValue() : 0);
					characteristicGroupSpecificationobj.getComponentCharacteristicGroupSpecification()
							.forEach(componentCharacteristicGroupSpecification -> {
								try {
									Node componentCharacteristicGroupSpecificationNode = characteristicGroupSpecificationNode
											.addNode(componentCharacteristicGroupSpecification.getName());
									createCharacteristicGroupSpecificationNode(
											componentCharacteristicGroupSpecificationNode, specification);
								}

						catch (ValueFormatException valueEx) {
									log.info("Error:" + valueEx.getLocalizedMessage());
									errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION, FetchErrorMessage
											.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

								} catch (LockException lockEx) {

									log.info("Error:" + lockEx.getLocalizedMessage());
									errorMap.put(ErrorCodeConstants.LOCK_EXEPTION, FetchErrorMessage
											.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

								} catch (RepositoryException repositoryEx) {
									log.info("Error:" + repositoryEx.getLocalizedMessage());
									errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE, FetchErrorMessage
											.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

								} catch (Exception ex) {

									log.info("Error:" + ex.getLocalizedMessage());
									errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage
											.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

								}

							});
				}
			}
			List<CharacteristicSpecification> characteristicSpecificationList = specification
					.getCharacteristicSpecification();
			if (!characteristicSpecificationList.isEmpty()) {
				
				//31/05
				for(CharacteristicSpecification charSpec:characteristicSpecificationList)
				{
					characteristicSpecification(node, charSpec);
				}
				
			}
			List<StatusHistory> statusHistoryList = specification.getHistory();
			if (!statusHistoryList.isEmpty()) {
				for (StatusHistory statusHistoryobj : statusHistoryList) {
					Node statusHistoryNode = node.addNode(RebusUtils.STATUS_HISTORY);
					statusHistoryNode.setProperty(RebusUtils.ID, statusHistoryobj.getId());
					statusHistoryNode.setProperty(RebusUtils.REASON, statusHistoryobj.getReason());
					statusHistoryNode.setProperty(RebusUtils.USER, statusHistoryobj.getUser());
					statusHistoryNode.setProperty(RebusUtils.TIME_STAMP,
							statusHistoryobj.getTimestamp().toGregorianCalendar());
					if (statusHistoryobj.getNewStatus() != null) {
						statusHistoryNode.addNode(RebusUtils.NEWSTATUS);
						if (statusHistoryobj.getOldStatus() != null) {
							statusHistoryNode.addNode(RebusUtils.OLDSTATUS);
						}
					}
				}
			}
				List<RelationshipSpecification> RelationshipSpecificationList = specification
						.getReferencedSpecification();
				if (!RelationshipSpecificationList.isEmpty()) {
					for (RelationshipSpecification relationshipSpecificationobj : RelationshipSpecificationList) {
						Node relationshipSpecificationNode = node.addNode(relationshipSpecificationobj.getName());
						entitySpecificationSetProperty(relationshipSpecificationNode, relationshipSpecificationobj);
						relationshipSpecificationNode.setProperty(RebusUtils.MIN_MULTIPICITY,
								relationshipSpecificationobj.getMinMultiplicity().longValue());
						relationshipSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,
								relationshipSpecificationobj != null
										? relationshipSpecificationobj.getMaxMultiplicity().longValue() : 0);
						relationshipSpecificationobj.getRelatedSpecification().forEach(relatedSpecification -> {
							try {
								specificationSetProperty(relationshipSpecificationNode, relatedSpecification);
							} catch (Exception ex) {
								log.info("Error:" + ex.getLocalizedMessage());
								errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage
										.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
							}

						});
					}
				}
				node.setProperty(RebusUtils.STORE_INVENTORY, specification.isStoreInventory());
				if (specification.getStatus() != null) {
					//Node statusNode = node.addNode(specification.getStatus().value());
					node.setProperty(RebusUtils.STATUS, specification.getStatus().value());
				}
			
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * priceItemSpec .
	 * 
	 * Passing pricePlanNode ,priceItemSpec
	 * 
	 * @param pricePlanNode
	 *            is used to add the property .
	 * @param priceItemSpec
	 *            object is used to iterate the List of PriceItemSpecification .
	 * 
	 * @return void
	 * 
	 */

	private void priceItemSpec(Node pricePlanNode, List<PriceItemSpecification> priceItemSpec) {
		try {
			if (!priceItemSpec.isEmpty()) {
				for (PriceItemSpecification priceItemSpecification : priceItemSpec) {
					Node priceItemSpecificationNode = pricePlanNode.addNode(priceItemSpecification.getName());
					entitySpecificationSetProperty(priceItemSpecificationNode, priceItemSpecification);
					priceItemSpecificationNode.setProperty(RebusUtils.MIN_MULTIPICITY,
							priceItemSpecification.getMinMultiplicity().longValue());
					priceItemSpecificationNode.setProperty(RebusUtils.MAX_MULTIPICITY,
							priceItemSpecification.getMaxMultiplicity() != null
									? priceItemSpecification.getMaxMultiplicity().longValue() : 0);
					if (priceItemSpecification.isUseForContractRenew() != null)
						priceItemSpecificationNode.setProperty(RebusUtils.IS_USABLE_FOR_CONTRACT_RENEW,
								priceItemSpecification.isUseForContractRenew());
					
					 ApplicabilitySpecification applicability =priceItemSpecification.getApplicability(); 
					  if(applicability != null) 
					  {
						  applicabilitySpecNode(priceItemSpecificationNode, applicability);
					  }
					 
					if (priceItemSpecification.getSingleViewDetails() != null) {
						Node singleViewDetailsNode = priceItemSpecificationNode.addNode(RebusUtils.SINGLE_VIEW_DETAILS,
								RebusUtils.NT_UNSTRUCTURED);
						List<AccountingSchemeSpecification> accountingSchemeSpecificationList = priceItemSpecification
								.getSingleViewDetails().getAccountingScheme();
						if (!accountingSchemeSpecificationList.isEmpty()) {
							for (AccountingSchemeSpecification accountingSchemeSpecification : accountingSchemeSpecificationList) {
								Node accountingSchemeSpecificationNode = singleViewDetailsNode
										.addNode(accountingSchemeSpecification.getName());
								entitySpecificationSetProperty(accountingSchemeSpecificationNode,
										accountingSchemeSpecification);
								List<AccountingItemSpecification> accountingItemSpecificationList = accountingSchemeSpecification
										.getItem();
								if (!accountingItemSpecificationList.isEmpty()) {
									for (AccountingItemSpecification accountingItemSpecification : accountingItemSpecificationList) {
										Node accountingItemSpecificationNode = accountingSchemeSpecificationNode
												.addNode(accountingItemSpecification.getName());
										entitySpecificationSetProperty(accountingItemSpecificationNode,
												accountingItemSpecification);
										if (accountingItemSpecification.getCreditAccount() != null)
											accountingItemSpecificationNode.setProperty(RebusUtils.CREDIT_AMOUNT,
													accountingItemSpecification.getCreditAccount());
										if (accountingItemSpecification.getDebitAccount() != null)
											accountingItemSpecificationNode.setProperty(RebusUtils.DEBIT_AMOUNT,
													accountingItemSpecification.getDebitAccount());
										accountingItemSpecificationNode.setProperty(RebusUtils.AMOUNT_PERCENTAGE,
												accountingItemSpecification.getAmountPercentage() != null
														? accountingItemSpecification.getAmountPercentage().intValue()
														: 0);
										if (accountingItemSpecification.getFixedAmount() != null) {
											Node fixedAmountNode = accountingItemSpecificationNode
													.addNode(RebusUtils.FIXED_AMOUNT, RebusUtils.NT_UNSTRUCTURED);
											fixedAmountNode.setProperty(RebusUtils.AMOUNT,
													accountingItemSpecification.getFixedAmount().getAmount());
											fixedAmountNode.setProperty(RebusUtils.CURRENCY,
													accountingItemSpecification.getFixedAmount().getCurrency());
										}
										if (accountingItemSpecification.getOffsetAmount() != null) {
											Node offsetAmountNode = accountingItemSpecificationNode
													.addNode(RebusUtils.OFFSET_AMOUNT, RebusUtils.NT_UNSTRUCTURED);
											offsetAmountNode.setProperty(RebusUtils.AMOUNT,
													accountingItemSpecification.getOffsetAmount().getAmount());
											offsetAmountNode.setProperty(RebusUtils.CURRENCY,
													accountingItemSpecification.getOffsetAmount().getCurrency());
										}
										if (accountingItemSpecification.getRevenueRecognition() != null)
											accountingItemSpecificationNode.setProperty(
													RebusUtils.REVENUE_RECOGNITION_TYPE,
													accountingItemSpecification.getRevenueRecognition().value());
									}
								}
							}
						}
					}
					if (priceItemSpecification.getTaxClass() != null) {
						Node taxRateClassNode = priceItemSpecificationNode
								.addNode(priceItemSpecification.getTaxClass().getName(), RebusUtils.NT_UNSTRUCTURED);
						entitySpecificationSetProperty(taxRateClassNode, priceItemSpecification.getTaxClass());
					}
					ValueExpression valueExpression = priceItemSpecification.getValueExpression();
					if (valueExpression != null) {
						Node valueExpressionNode = priceItemSpecificationNode.addNode(valueExpression.getName(),
								RebusUtils.NT_UNSTRUCTURED);
						entitySpecificationSetProperty(valueExpressionNode, valueExpression);
						List<ValueExpressionItem> valueExpressionItemList = valueExpression.getItem();
						if (!valueExpressionItemList.isEmpty()) {
							for (ValueExpressionItem valueExpressionItem : valueExpressionItemList) {
								Node valueExpressionItemNode = valueExpressionNode
										.addNode(RebusUtils.VALUE_EXPRESSION_ITEM);
								valueExpressionItemNode.setProperty(RebusUtils.SEQUENCE,
										valueExpressionItem.getSequence().intValue());
							}
						}
					}
					List<PriceItemSpecification> referencedPriceItemList = priceItemSpecification
							.getReferencedPriceItem();
					if (!referencedPriceItemList.isEmpty()) {
						for (PriceItemSpecification referencedPriceItemSpec : referencedPriceItemList) {
							Node referencedPriceItemNode = priceItemSpecificationNode
									.addNode(referencedPriceItemSpec.getName(), RebusUtils.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(referencedPriceItemNode, referencedPriceItemSpec);
							if (referencedPriceItemSpec != null) {
								priceItemSpec(priceItemSpecificationNode, referencedPriceItemList);
							}
						}
					}

					if (priceItemSpecification instanceof ChargeSpecification) {
						ChargeSpecification chargeSpecification = (ChargeSpecification) priceItemSpecification;
						if (chargeSpecification != null) {
							priceItemSpecificationNode.setProperty(RebusUtils.IS_AMOUNT_EDITABLE,
									chargeSpecification.isAmountEditable());
							Money amount = chargeSpecification.getAmount();
							if (amount != null)
								createAmountNode(priceItemSpecificationNode, amount, RebusUtils.AMOUNT_NODE);
							SpendingUnit spendingUnits = chargeSpecification.getSpendingUnits();
							if (spendingUnits != null) {
								if (!priceItemSpecificationNode.hasNode(spendingUnits.getName())) {
									Node spendingUnitsNode = priceItemSpecificationNode.addNode(spendingUnits.getName(),
											RebusUtils.NT_UNSTRUCTURED);
									entitySpecificationSetProperty(spendingUnitsNode, spendingUnits);
								}
							}
							ChargeType chargeType = chargeSpecification.getChargeType();
							if (chargeType != null) {
								Node chargeTypeNode = priceItemSpecificationNode.addNode(chargeType.getName(),
										RebusUtils.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(chargeTypeNode, chargeType);
							}
							BillingCycleSpecification billingCycle = chargeSpecification.getBillingCycle();
							if (billingCycle != null) {
								Node billingCycleNode = priceItemSpecificationNode.addNode(RebusUtils.BILLING_CYCLE);
								billingCycleNode.setProperty(RebusUtils.IS_INVOICE, billingCycle.isInvoice());
								billingCycleNode.setProperty(RebusUtils.RECURRING, billingCycle.isRecurring());
								if (billingCycle.getPeriodicityUnits() != null)
									billingCycleNode.setProperty(RebusUtils.PERIODICITY_UNITS,
											billingCycle.getPeriodicityUnits().intValue());
								if (billingCycle.getTotalCycles() != null)
									billingCycleNode.setProperty(RebusUtils.TOTAL_CYCLES,
											billingCycle.getTotalCycles().intValue());
								if (billingCycle.getPeriodicity() != null) {
									Node periodicityTypeNode = billingCycleNode
											.addNode(billingCycle.getPeriodicity().getName());
									entitySpecificationSetProperty(periodicityTypeNode, billingCycle.getPeriodicity());
								}
							}
							SpendingLimitSpecification spendingLimit = chargeSpecification.getSpendingLimit();
							if (spendingLimit != null) {
								Node spendingLimitNode = priceItemSpecificationNode.addNode(RebusUtils.SPENDING_LIMIT);
								if (spendingLimit.isLimitEditable() != null)
									spendingLimitNode.setProperty(RebusUtils.IS_LIMIT_EDITABLE,
											spendingLimit.isLimitEditable());
								SpendingLimitRule spendinglimitRule = spendingLimit.getRule();
								if (spendinglimitRule != null) {
									Node spendinglimitRuleNode = spendingLimitNode
											.addNode(RebusUtils.SPENDING_LIMIT_RULE);
									List<BigDecimal> alertList = spendinglimitRule.getAlert();
									if (!alertList.isEmpty()) {
										for (BigDecimal alert : alertList) {
											Node alertNode = priceItemSpecificationNode.addNode(RebusUtils.ALERT_NODE);
											alertNode.setProperty(RebusUtils.ALERT, alert.intValue());
										}
									}
									Money limit = spendinglimitRule.getLimit();
									if (limit != null)
										createAmountNode(spendinglimitRuleNode, limit, RebusUtils.LIMIT);
									ApplicabilityRule applicabilityRule = spendinglimitRule.getApplicability();
									if (applicabilityRule != null)
										applicabilityRuleNode(spendinglimitRuleNode, applicabilityRule);
								}
							}
						}
					} else if (priceItemSpecification instanceof AllowanceSpecification) {
						AllowanceSpecification allowanceSpecification = (AllowanceSpecification) priceItemSpecification;
						if (allowanceSpecification != null) {
							priceItemSpecificationNode.setProperty(RebusUtils.IS_TOTAL_ALLOWANCE_EDITABLE,
									allowanceSpecification.isTotalAllowanceEditable());
							BigInteger totalSpendingUnits = allowanceSpecification.getTotalSpendingUnits();
							if (totalSpendingUnits != null)
								priceItemSpecificationNode.setProperty(RebusUtils.TOTAL_SPENDING_UNITS,
										totalSpendingUnits.longValue());
							if (allowanceSpecification.getTotalFinancial() != null)
								createAmountNode(priceItemSpecificationNode, allowanceSpecification.getTotalFinancial(),
										RebusUtils.TOTAL_FINANCIAL);
							SpendingUnit spendingUnits = allowanceSpecification.getSpendingUnits();
							if (spendingUnits != null) {
								Node spendingUnitsNode = priceItemSpecificationNode.addNode(spendingUnits.getName(),
										RebusUtils.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(spendingUnitsNode, spendingUnits);
							}
							AllowancePeriodicity periodicity = allowanceSpecification.getPeriodicity();
							if (periodicity != null) {
								Node periodicityNode = priceItemSpecificationNode.addNode(RebusUtils.PERIODICITY_NODE);
								if (periodicity.getPeriodicityUnits() != null)
									periodicityNode.setProperty(RebusUtils.PERIODICITY_UNITS,
											periodicity.getPeriodicityUnits().intValue());
								if (periodicity.getTotalCycles() != null)
									periodicityNode.setProperty(RebusUtils.TOTAL_CYCLES,
											periodicity.getTotalCycles().intValue());
								if (periodicity.isRecurring() != null)
									periodicityNode.setProperty(RebusUtils.RECURRING, periodicity.isRecurring());
								if (periodicity.getPerioditicy() != null) {
									Node periodicityTypeNode = periodicityNode
											.addNode(periodicity.getPerioditicy().getName());
									entitySpecificationSetProperty(periodicityTypeNode, periodicity.getPerioditicy());
								}
							}
							List<BigDecimal> alertList = allowanceSpecification.getAlert();
							if (!alertList.isEmpty()) {
								for (BigDecimal alert : alertList) {
									Node alertNode = priceItemSpecificationNode.addNode(RebusUtils.ALERT_NODE);
									alertNode.setProperty(RebusUtils.ALERT, alert.intValue());
								}
							}
							if (allowanceSpecification.isFlatRate() != null)
								priceItemSpecificationNode.setProperty(RebusUtils.FLAT_RATE,
										allowanceSpecification.isFlatRate());
							ApplicabilitySpecification overUse = allowanceSpecification.getApplicability();
							if (overUse != null) {
								applicabilitySpecNode(priceItemSpecificationNode, overUse);
							}
							AllowanceGroupRelationshipSpecification referencedAllowanceGroup = allowanceSpecification
									.getReferencedAllowanceGroup();
							if (referencedAllowanceGroup != null) {
								Node referencedAllowanceGroupNode = priceItemSpecificationNode
										.addNode(RebusUtils.REFERENCED_ALLOWANCE_GROUP);
								referencedAllowanceGroupNode.setProperty(RebusUtils.SPENDING_UNITS_FOR_PRICE,
										referencedAllowanceGroup.getSpendingUnitsForPrice().intValue());
								Money price = referencedAllowanceGroup.getPrice();
								if (price != null)
									createAmountNode(referencedAllowanceGroupNode, price, RebusUtils.PRICE);
								AllowanceGroupSpecification allowanceGroupSpecification = referencedAllowanceGroup
										.getGroup();
								if (allowanceGroupSpecification != null)
									createAllowancegroupSpecNode(referencedAllowanceGroupNode,
											allowanceGroupSpecification);
							}
							RolloverSpecification rolloverSpecification = allowanceSpecification
									.getRolloverSpecification();
							if (rolloverSpecification != null) {
								Node rolloverSpecificationNode = priceItemSpecificationNode
										.addNode(RebusUtils.ROLL_OVER);
								rolloverSpecificationNode.setProperty(RebusUtils.PERIODICITY_UNITS,
										rolloverSpecification.getPeriodicityUnits().intValue());
								if (rolloverSpecification.getPeriodicity() != null) {
									Node periodicityTypeNode = rolloverSpecificationNode
											.addNode(rolloverSpecification.getPeriodicity().getName());
									entitySpecificationSetProperty(periodicityTypeNode,
											rolloverSpecification.getPeriodicity());
								}
							}
						}
					} else if (priceItemSpecification instanceof DiscountSpecification) {
						DiscountSpecification discountSpecification = (DiscountSpecification) priceItemSpecification;
						if (discountSpecification != null) {
							priceItemSpecificationNode.setProperty(RebusUtils.DISCOUNT,
									discountSpecification.getDiscount());
						}
					} else if (priceItemSpecification instanceof CommissionSpecification) {
						CommissionSpecification commissionSpecification = (CommissionSpecification) priceItemSpecification;
						Money amount = commissionSpecification.getAmount();
						if (amount != null)
							createAmountNode(priceItemSpecificationNode, amount, RebusUtils.AMOUNT_NODE);
						if (commissionSpecification.getCommissionEvent() != null)
							priceItemSpecificationNode.setProperty(RebusUtils.COMMISSION_EVENT,
									commissionSpecification.getCommissionEvent());
						if (commissionSpecification.getCommissionType() != null)
							priceItemSpecificationNode.setProperty(RebusUtils.COMMISSION_TYPE,
									commissionSpecification.getCommissionType());
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the AllowancegroupSpecNode and set the related
	 * properties
	 * 
	 * Passing node ,allowanceGroupSpecification
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param allowanceGroupSpecification
	 *            object is used to hold the allowanceGroupSpecification data .
	 * 
	 * @return void
	 * 
	 */

	private void createAllowancegroupSpecNode(Node node, AllowanceGroupSpecification allowanceGroupSpecification) {
		try {
			Node allowanceGroupSpecificationNode = node.addNode(allowanceGroupSpecification.getName());
			entitySpecificationSetProperty(allowanceGroupSpecificationNode, allowanceGroupSpecification);
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the Nodes and properties which are related to
	 * EligibilityRule
	 * 
	 * Passing node ,spec
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param spec
	 *            object is used to hold the specification data .
	 * 
	 * @return void
	 * 
	 */

	protected void createEligibilityRuleNode(Node node, Specification spec) {
		try {
			List<EligibilityRule> eligibilityRuleList = null;
			if (spec instanceof ProductOfferingSpecification) {
				eligibilityRuleList = ((ProductOfferingSpecification) spec).getEligibilityRule();
			} else if (spec instanceof PricePlanSpecification) {
				eligibilityRuleList = ((PricePlanSpecification) spec).getEligibilityRule();
			}
			if (!eligibilityRuleList.isEmpty()) {
				for (EligibilityRule eligibilityRule : eligibilityRuleList) {
					Node eligibilityRuleNode = node.addNode(eligibilityRule.getName(), RebusUtils.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(eligibilityRuleNode, eligibilityRule);
					ConditionExpression conditionexpressionOfEligibility = eligibilityRule.getConditionExpression();
					if (conditionexpressionOfEligibility != null) {
						createConditionExpressionNode(eligibilityRuleNode, conditionexpressionOfEligibility);
					}
					if (!eligibilityRule.getCustomerSegment().isEmpty()) {
						for (CustomerSegment customerSegment : eligibilityRule.getCustomerSegment()) {
							Node customerSegmentNode = eligibilityRuleNode.addNode(customerSegment.getName());
							List<MarketSegment> marketSegmentList = customerSegment.getMarketSegment();
							if (marketSegmentList != null) {
								for (MarketSegment marketSegmentObj : marketSegmentList) {
									Node marketSegmentNode = customerSegmentNode.addNode(marketSegmentObj.getName(),
											RebusUtils.NT_UNSTRUCTURED);
									entitySpecificationSetProperty(marketSegmentNode, marketSegmentObj);
								}
							}
						}
					}
					List<MarketSegment> marketSegment = eligibilityRule.getMarketSegment();
					if (!marketSegment.isEmpty()) {
						for (MarketSegment marketSegmentObj : marketSegment) {
							Node marketSegmentNode = eligibilityRuleNode.addNode(marketSegmentObj.getName(),
									RebusUtils.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(marketSegmentNode, marketSegmentObj);
						}
					}
					List<SellingChannel> sellingChannelList = eligibilityRule.getSellingChannel();
					if (!sellingChannelList.isEmpty()) {
						for (SellingChannel sellingChannel : sellingChannelList) {
							eligibilityRuleNode.addNode(sellingChannel.getName(), RebusUtils.NT_UNSTRUCTURED);
						}
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}
}
