package uk.co.three.rebus.three_rebus.core.request;

import javax.jcr.Node;
import javax.jcr.Session;

import uk.co.three.rebus.three_rebus.core.response.ProductDataGetResponse;

/**
 * @author MS00506946
 *
 */
public class ProductOfferRequest extends BaseRequest {

	private ProductDataGetResponse productDataGetResponse;

	private Node catalogNode;

	private Session session;

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session
	 *            the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}

	/**
	 * @return the productDataGetResponse
	 */
	public ProductDataGetResponse getProductDataGetResponse() {
		return productDataGetResponse;
	}

	/**
	 * @param productDataGetResponse
	 *            the productDataGetResponse to set
	 */
	public void setProductDataGetResponse(ProductDataGetResponse productDataGetResponse) {
		this.productDataGetResponse = productDataGetResponse;
	}

	/**
	 * @return the catalogNode
	 */
	public Node getCatalogNode() {
		return catalogNode;
	}

	/**
	 * @param catalogNode
	 *            the catalogNode to set
	 */
	public void setCatalogNode(Node catalogNode) {
		this.catalogNode = catalogNode;
	}

}
