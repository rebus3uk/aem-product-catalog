/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.response;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;

import epc.h3g.com.OfferingsDetails;

/**
 * Specfic Response class for getting offering details
 * 
 * @author MS00506946
 *
 */
public class ProductDataGetResponse extends BaseResponse {

	public OfferingsDetails offeringsDetails;

	public Node catalogNode;

	public Session session;
	public ResourceResolver resourceResolver;

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session
	 *            the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}

	/**
	 * @return the resourceResolver
	 */
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	/**
	 * @param resourceResolver
	 *            the resourceResolver to set
	 */
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	/**
	 * @return the catalogNode
	 */
	public Node getCatalogNode() {
		return catalogNode;
	}

	/**
	 * @param catalogNode
	 *            the catalogNode to set
	 */
	public void setCatalogNode(Node catalogNode) {
		this.catalogNode = catalogNode;
	}

	/**
	 * @return the offeringsDetails
	 */
	public OfferingsDetails getOfferingsDetails() {
		return offeringsDetails;
	}

	/**
	 * @param offeringsDetails
	 *            the offeringsDetails to set
	 */
	public void setOfferingsDetails(OfferingsDetails offeringsDetails) {
		this.offeringsDetails = offeringsDetails;
	}

}
