package uk.co.three.rebus.three_rebus.core.service;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolver;

import epc.h3g.com.OfferingsDetails;

public interface ProductImporterService {
	public void doImport() ;
	
	//public void AddProductObject(OfferingsDetails offeringsDetailsObj);

}
