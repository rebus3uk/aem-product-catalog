package uk.co.three.rebus.three_rebus.core.service;

import javax.jcr.Node;
import javax.jcr.Session;

import epc.h3g.com.OfferingsDetails;
import uk.co.three.rebus.three_rebus.core.request.BaseRequest;
import uk.co.three.rebus.three_rebus.core.response.BaseResponse;

public interface OfferingsDetailsProvider {
	
	public BaseResponse  getOfferingsDetails(Session session );
	
	public void offerDetailsProcessor(Node catalogNode,OfferingsDetails offeringsDetailsObj);
	

}
