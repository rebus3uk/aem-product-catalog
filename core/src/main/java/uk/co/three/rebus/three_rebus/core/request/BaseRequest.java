/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.request;

import java.io.Serializable;

import org.apache.sling.api.SlingHttpServletRequest;

/**
 * This is the base request class for the entire project
 * This class does not have contents as of now
 * All request classes will extend from this class
 * @author GS00355198
 *
 */
public class BaseRequest implements Serializable {
	
	
	private String resourcePath = "";
	
	private SlingHttpServletRequest  slingRequest = null; 
	 
	public BaseRequest(){
		
	}
	 
	 

	public String getResourcePath() {
		return resourcePath;
	}



	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}



	public SlingHttpServletRequest getSlingRequest() {
		return slingRequest;
	}

	public void setSlingRequest(SlingHttpServletRequest slingRequest) {
		this.slingRequest = slingRequest;
	}
	
	
	
	
	
	

}
