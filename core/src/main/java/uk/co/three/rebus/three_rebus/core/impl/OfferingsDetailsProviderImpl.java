package uk.co.three.rebus.three_rebus.core.impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.Category;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CommercialExport;
import epc.h3g.com.OfferingsDetails;
import epc.h3g.com.ProductOfferingSpecification;
import uk.co.three.rebus.three_rebus.core.request.BaseRequest;
import uk.co.three.rebus.three_rebus.core.request.CatalogRequest;
import uk.co.three.rebus.three_rebus.core.response.BaseResponse;
import uk.co.three.rebus.three_rebus.core.response.CatalogResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;

@Component(label = "Title", description = "Description.", immediate = true, metatype = true)//, policy = ConfigurationPolicy.REQUIRE
@Service(value = {OfferingsDetailsProvider.class})

public class OfferingsDetailsProviderImpl  implements OfferingsDetailsProvider {

	private static final Logger log = LoggerFactory.getLogger(OfferingsDetailsProviderImpl.class);
	

	
	public OfferingsDetails getOfferingsDetails() {
		// TODO Auto-generated method stub
	//	OfferingsDetails offeringsDetails=populateOfferingsDetails();
		
		// log.info("offeringsDetails==="+offeringsDetails);
	/*	 if(offeringsDetails.getProductOfferingRelationship() != null && !offeringsDetails.getProductOfferingRelationship().isEmpty())
			 log.info("offeringsDetails list==="+offeringsDetails.getProductOfferingRelationship());
		 else if(offeringsDetails.getProductOfferingRelationship() != null)
			 log.info("offeringsDetails list is  empty===");
		 else
			 log.info("offeringsDetails list is  null===");
		 */
		 
		return null;
	}
	
	@Activate
	protected void activate(Map<String, String> config) {
	    log.info("OfferingsDetailsProviderImpl ACTIVATED");
	  
	
	}
	
	private OfferingsDetails populateOfferingsDetails(InputStream stream){
		log.info("");
		log.info("--inside populateOfferingsDetails--");
		//final String xmlPath = "//uk//co//three//rebus//three_rebus//core//impl//Singleview_essential_plan.xml";//"F:\\Rebus\\catalog\\IBMOM_essential_plan.xml";
		//final String xmlPath = "resource/Singleview_essential_plan.xml";//"F:\\Rebus\\catalog\\IBMOM_essential_plan.xml";

		File xmlFile = null;
		
		/*try {
			xmlFile = new File(xmlPath);
		} catch (Exception e1) {
			System.out.println(e1.getLocalizedMessage());
		}
		if(xmlFile == null || !xmlFile.exists()){
			System.out.println("xml file does not exist");
			return null;
		}*/
		//InputStream stream = OfferingsDetailsProviderImpl.class.getResourceAsStream("Singleview_essential_plan.xml");
		/*InputStream stream = null;
		try {
			NodeIterator ni=nodeObj.getNodes();
			
			StringBuilder output = new StringBuilder();
		    while (nodeObj.getNodes().hasNext()) {
		      Node node = nodeObj.getNodes().nextNode();
		      output.append("path=" + node.getPath() + "\n");
		    }
			
			
		Node jcrContent = null;
		
		log.info("jcrContent="+jcrContent);
		log.info("jcrContent ===>"+ jcrContent.getPath());
		
		stream = jcrContent.getProperty("jcr:data").getBinary().getStream();
		
		log.info("stream ===>"+ stream);
		
		} catch (ValueFormatException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (PathNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (RepositoryException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}*/
		 
		XMLInputFactory xif = XMLInputFactory.newFactory();
       // StreamSource xml = new StreamSource(xmlPath);
        XMLStreamReader xsr = null;
       
		try {
			//xsr = xif.createXMLStreamReader(xml);
			
			xsr = xif.createXMLStreamReader(stream);
			log.info("xsr="+xsr);
			xsr.nextTag();
			log.info("xsr.getLocalName():: "+ xsr.getLocalName());
			while (!xsr.getLocalName().equals("commercialExport")) {
				
			
				xsr.nextTag();
			}
		} catch (XMLStreamException e1) {
			System.out.println(e1.getLocalizedMessage());
		}
        
       
		
		
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(CommercialExport.class);
			
			log.info("context:: "+ context.toString());
			//context = JAXBContext.newInstance();
		} catch (JAXBException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		if(context == null){
			System.out.println("Failed to create context");
			return null;
		}
		
		Unmarshaller unmarshaller = null;
		try {
			unmarshaller = context.createUnmarshaller();
		} catch (JAXBException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		if(unmarshaller == null){
			System.out.println("failed to create unmarshaller instance");
			return null;
		}
		
		CommercialExport comExport = null;
		JAXBElement<CommercialExport> jbElement = null;
		try {
			/*comExport = (CommercialExport)unmarshaller.unmarshal(xmlFile);*/
			jbElement = unmarshaller.unmarshal(xsr,CommercialExport.class);
			
			System.out.println(jbElement.getName().getNamespaceURI());
			
			
		} catch (JAXBException e) {
			System.out.println(e.getLocalizedMessage());
		}/*finally{
			try {
				xsr.close();
			} catch (XMLStreamException e) {
				// cannot do much
			}
		}*/
		
		if(jbElement == null){
			System.out.println("Failed to read the xml file");
			return null;
		}
		
		
		
		
		
		System.out.println("Success in reading xml file content");
		CommercialExport value = jbElement.getValue();
		OfferingsDetails offeringsDetails = value.getOfferingsDetails();
		
		return offeringsDetails;
		
	}

	@Override
	public BaseResponse getOfferingsDetails(Session session) {
	     // ResourceResolver resourceResolver=baseRequestObj.getSlingRequest().getResourceResolver();
	      BaseResponse baseResponseObj = new BaseResponse();
		// Resource resource =baseRequestObj.getSlingRequest().getResourceResolver().getResource(baseRequestObj.getResourcePath());
try{
	
	//Session session = (Session) resourceResolver.adaptTo(Session.class);
	Node root = session.getRootNode();
	Node jcrContent = root.getNode("content/abc.xml/jcr:content");
	//OfferingsDetails offeringsDetailsObj = populateOfferingsDetails(jcrContent);
	
	InputStream stream = jcrContent.getProperty("jcr:data").getBinary().getStream();
	
	OfferingsDetails offeringsDetailsObj = populateOfferingsDetails(stream);
	baseResponseObj.setOfferingsDetailsObj(offeringsDetailsObj);
System.out.println("hi");
	/*BufferedInputStream bis = new BufferedInputStream(is);
	ByteArrayOutputStream buf = new ByteArrayOutputStream();
	int result = bis.read();
	while (result != -1) {
	    byte b = (byte) result;
	    buf.write(b);
	    result = bis.read();
	}

	System.out.println("plain text: " + buf.toString());
	System.out.println("hi");*/
	
}catch(Exception e)
{
	
	System.out.println(e.getMessage());

}
		
		
		
		// TODO Auto-generated method stub
		//"content/dam/geometrixx/portraits/scott_reynolds.jpg/jcr:content/metadata"
		//log.info("");
		//log.info("--inside getOfferingsDetails--");
		// Resource resource =baseRequestObj.getSlingRequest().getResourceResolver().getResource(baseRequestObj.getResourcePath());
		// log.info("resource="+resource);
	//	 BaseResponse baseResponseObj = new BaseResponse();

		// Node nodeObj= resource.adaptTo(Node.class);
		
		 
		// log.info("offeringsDetailsObj="+offeringsDetailsObj);
		// ((CatalogResponse)baseResponseObj).setOfferingsDetailsObj(offeringsDetailsObj);
		/// log.info("baseResponseObj="+baseResponseObj);
		return baseResponseObj;
	}

	@Override
	public void offerDetailsProcessor(Node catalogNode,OfferingsDetails offeringsDetailsObj) {
		// TODO Auto-generated method stub
		try
		{
			if (offeringsDetailsObj != null) {
				List<ProductOfferingSpecification> productOfferingSpecification = offeringsDetailsObj
						.getProductOfferingSpecification();

				productOfferingSpecification.stream().filter(productOfferSpec->productOfferSpec!=null).forEach(productOfferSpec -> {

					Category(catalogNode,productOfferSpec.getCategory(),productOfferSpec);

					productOfferSpec.getReferencedSpecification().stream().filter(relationshipSpecification->relationshipSpecification!=null).forEach(relationshipSpecification -> {

						relationshipSpecification.getRelatedSpecification().stream().filter(specification->specification!=null).forEach(specification -> {
							ProductOfferingSpecification sp = (ProductOfferingSpecification) specification;
							Category(catalogNode,sp.getCategory(),sp);

						});

					});
				});

			}
			
		}catch(Exception e)
		{
			
			
		}
		
		
		
		
	}
	
	/**
	 * Passing List<Category> categoryList.
	 * 
	 * @param List<Category>
	 *            categoryList we need to iterate
	 * 
	 *            And its call subCategory() method if we have subcategory in
	 *            the current iterating object
	 * @throws RepositoryException 
	 * @throws LockException 
	 * @throws ConstraintViolationException 
	 * @throws VersionException 
	 * @throws PathNotFoundException 
	 * @throws ItemExistsException 
	 * 
	 */
	public void Category(Node catalogNode,List<Category> categoryList,ProductOfferingSpecification productOfferSpec) {
		
		try
		{
			
			categoryList.stream().filter(category->category!=null).forEach(category -> {
				 Node categoryNode=null;
				try {
					if (!catalogNode.hasNode(category.getName())) {
						//categoryNode = catalogNode.addNode(category.getName());
						categoryNode = catalogNode.addNode(category.getName(), "sling:Folder");
					}
					else {
						
						categoryNode = catalogNode.getNode(category.getName());
						
						
					}
					
					
					if (!category.getSubcategory().isEmpty()) {

						subCategory(categoryNode,category.getSubcategory(),productOfferSpec);

					}
					
				} catch (ItemExistsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PathNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (VersionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ConstraintViolationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LockException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				

			});
			
			
		}catch(Exception e)
		{
			
			
		}
		   
			
	}

	/**
	 * Passing List<Category> categoryList.
	 * 
	 * @param List<Category>
	 *            categoryList we need to iterate
	 * 
	 *            recursive call to handle the scenario of Category inside
	 *            subCategory
	 * 
	 */
	public void subCategory(Node adobe ,List<Category> categoryList,ProductOfferingSpecification productOfferSpec) {

			categoryList.stream().filter(subCategory->subCategory!=null).forEach(subCategory -> {
				 Node subcategoryNode = null;
					try {

						if (!adobe.hasNode(subCategory.getName())) {
							subcategoryNode = adobe.addNode(subCategory.getName(),"sling:Folder");
							//subcategoryNode.addNode(productOfferSpec.getName());
						}
						else {
							
							subcategoryNode = adobe.getNode(subCategory.getName());
							
							
						}
						XMLGregorianCalendar xmlCalendar = subCategory.getValidFrom();
						subcategoryNode.setProperty("SubCategoryID", subCategory.getId());
						subcategoryNode.setProperty("SubCategoryType", subCategory.getType());
						Calendar calendar = xmlCalendar.toGregorianCalendar();
						
				    subcategoryNode.setProperty("SubCategoryValidFrom", calendar);
					System.out.println("SUBCategory ID " + subCategory.getId());
					System.out.println("SUBCategory Name" + subCategory.getName());
					System.out.println("SUBCategory Type" + subCategory.getType());
					System.out.println("SUBCategory ValidFrom" + subCategory.getValidFrom());

					if (!subCategory.getSubcategory().isEmpty()) {

						subCategory(subcategoryNode,subCategory.getSubcategory(),productOfferSpec);
											}
					else {
						Node productoffering = subcategoryNode.addNode(productOfferSpec.getName(),"nt:unstructured");
						productoffering.addNode(productOfferSpec.getAccountType().getName());
						ProductOfferingSpec(productOfferSpec,productoffering);
						
					}
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

			});
	}

	private void ProductOfferingSpec(
			ProductOfferingSpecification productOfferSpec, Node productoffering) {
		
		try
		{
			productOfferSpec.getPricePlanSpecification().stream().filter(pricePlanSpecification->pricePlanSpecification!=null).forEach(pricePlanSpecification->{
				try
				{
					Node pricePlanSpecificationNode=productoffering.addNode(pricePlanSpecification.getName());
					pricePlanSpecificationNode.setProperty("catalogId", pricePlanSpecification.getCatalogId());
					pricePlanSpecificationNode.setProperty("id", String.valueOf(pricePlanSpecification.getId()));
					pricePlanSpecificationNode.setProperty("type", pricePlanSpecification.getType());
					pricePlanSpecificationNode.setProperty("validFrom", String.valueOf(pricePlanSpecification.getValidFrom()));

					
					//pricePlanSpecificationNode.setProperty("CatalogId", pricePlanSpecification.getCatalogId());
					
					System.out.println("PricePlanSpecification***  "+pricePlanSpecification.getCatalogId());
					//CharacteristicSpecification(pricePlanSpecificationNode,pricePlanSpecification.getItem());
					
					
				}catch(Exception e)
				{
					e.getMessage();
					
				}
				
				
				
				
			});
			
		
			
			if(productOfferSpec.getProductSpecification()!=null)
			{
				
				
					Node productSpecificationNode=productoffering.addNode(productOfferSpec.getProductSpecification().getName());
					productSpecificationNode.setProperty("catalogId", productOfferSpec.getProductSpecification().getCatalogId());
					productSpecificationNode.setProperty("id", String.valueOf(productOfferSpec.getProductSpecification().getId()));
					productSpecificationNode.setProperty("type", productOfferSpec.getProductSpecification().getType());
					productSpecificationNode.setProperty("validFrom", String.valueOf(productOfferSpec.getProductSpecification().getValidFrom()));
					
					System.out.println("ProductSpecification***   "+productOfferSpec.getProductSpecification().getCatalogId());
					
					CharacteristicSpecification(productSpecificationNode,productOfferSpec.getProductSpecification().getCharacteristicSpecification());
				
			}
			if(productOfferSpec.getVersion()!=null)
			{
				
					Node versionNode=productoffering.addNode(productOfferSpec.getVersion().getName());
					versionNode.setProperty("catalogId", productOfferSpec.getVersion().getCatalogId());
					versionNode.setProperty("id", String.valueOf(productOfferSpec.getVersion().getId()));
					versionNode.setProperty("type", productOfferSpec.getVersion().getType());
					versionNode.setProperty("validFrom", String.valueOf(productOfferSpec.getVersion().getValidFrom()));
					
					System.out.println("Version***    "+productOfferSpec.getVersion().getCatalogId());
					
				
				
				

			}
			
			
			productOfferSpec.getEligibilityRule().stream().filter(eligibilityRule->eligibilityRule!=null).forEach(eligibilityRule->{
				
				try
				{
					Node eligibilityRuleNode=productoffering.addNode(eligibilityRule.getName());
					eligibilityRuleNode.setProperty("catalogId", eligibilityRule.getCatalogId());
					eligibilityRuleNode.setProperty("id", String.valueOf(eligibilityRule.getId()));
					eligibilityRuleNode.setProperty("type", eligibilityRule.getType());
					eligibilityRuleNode.setProperty("validFrom", String.valueOf(eligibilityRule.getValidFrom()));
					
					System.out.println("eligibilityRule*** "+eligibilityRule.getCatalogId());
					
				}catch(Exception e)
				{
					e.getMessage();
					
				}
				
				
				
				
			});
			
			
		}catch(Exception e)
		{
			
			e.getMessage();
			
		}
		
		
		
		
		CharacteristicSpecification(productoffering,productOfferSpec.getCharacteristicSpecification());
		
	}

	private void CharacteristicSpecification(Node productoffering,
			List<CharacteristicSpecification> listChar) {
		listChar.stream().filter(characteristicSpecification->characteristicSpecification!=null).forEach(characteristicSpecification->{
			
			
			try
			{
				Node characteristicSpecificationNode=productoffering.addNode(characteristicSpecification.getName());
				//pricePlanSpecificationNode.setProperty("CatalogId", pricePlanSpecification.getCatalogId());
				characteristicSpecificationNode.setProperty("catalogId", characteristicSpecification.getCatalogId());
				characteristicSpecificationNode.setProperty("id", String.valueOf(characteristicSpecification.getId()));
				characteristicSpecificationNode.setProperty("type", characteristicSpecification.getType());
				characteristicSpecificationNode.setProperty("validFrom", characteristicSpecification.getValidFrom().toGregorianCalendar());
				System.out.println("characteristicSpecification***  "+characteristicSpecification.getCatalogId());
				
			}catch(Exception e)
			{
				e.getMessage();
				
			}
			
			
			
			
		});
		
		
	}

	
		
		
		
		
		
	}

	
	
	


