package uk.co.three.rebus.three_rebus.core.service;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.three.rebus.three_rebus.core.response.BaseResponse;
import uk.co.three.rebus.three_rebus.core.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.service.ProductImporterService;




@Component(immediate=true)
@Service(value = {ProductImporterService.class})
public class ThreeProductImporter implements ProductImporterService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference 
    private ResourceResolverFactory resourceFactory;
    
    @Reference
    private OfferingsDetailsProvider offeringsDetailsProvider;
    
  
    
	
	
	protected String absolutePath = "/content/three_rebus/products";
	protected String storeName = "threeRebus";

	private String productPath;
	
	@Override
	@Activate
	public void doImport() {
		log.info("resourceFactory=="+resourceFactory);
		Map<String, Object> productImp = new HashMap<String, Object>();
        
		productImp.put(ResourceResolverFactory.SUBSERVICE, "readService");
        log.info("After the param");
        ResourceResolver rr = null;
        try{
              rr = resourceFactory.getServiceResourceResolver(productImp);
              Session session = rr.adaptTo(Session.class);
              Resource resource = rr.getResource(absolutePath);
              log.info("**************************");
  			Node storeRoot = resource.adaptTo(Node.class);

  			if (!storeRoot.hasNode(storeName)) {
  				addStoreName(session,rr,absolutePath);

  			}else {
  				productPath = absolutePath+"/"+storeName;
				addProduct(session,rr, productPath);
			}
        }catch (RepositoryException e) {

			e.printStackTrace();
		} catch (LoginException e) {

			e.printStackTrace();
		}
		
		
		
		
	}
	public String addStoreName(Session session, ResourceResolver resourceResolver,String absolutePath) {
		
		
		try {
			
			session = resourceResolver.adaptTo(Session.class);
           
			Node productsNode = session.getNode(absolutePath);
			log.info("***** Parent of Store Name ******" + productsNode.getName());
			
			Node storeNode = productsNode.addNode(storeName);
			
			session.save();
			
			productPath = storeNode.getPath();
			
			log.info("<<<<< Three Rebus Store Path >>>>>>>>>>" + productPath);
			
			addProduct(session, resourceResolver, productPath);
			

		} catch (Exception e) {
			//log.error("Failed to create Product Store" + e.getMessage());
			return "Failed to create Product Store" + e.getMessage();
		}
		return "Store has been created successfully";

	}
	public String addProduct(Session session, ResourceResolver resourceResolver, String productPath) {
		
		
		
		try {
			
			

			Node catalogNode = session.getNode(productPath);
			
			log.info("<<<<< Store Name >>>>>>>>>>" + catalogNode.getName());
			
			BaseResponse baseResponse = new BaseResponse();
			 baseResponse= offeringsDetailsProvider.getOfferingsDetails(session);
			 //node create
			 offeringsDetailsProvider.offerDetailsProcessor(catalogNode,baseResponse.getOfferingsDetailsObj());
			 
			 
			
			if(baseResponse.getOfferingsDetailsObj()==null){
				
				log.info("<<<<< We are getting product catalog data >>>>>>>>>>" );
			}
			
			
			
			
			
			//productNode.addNode("Debal");
			 session.save();
			
		} catch (PathNotFoundException e) {
			
			return "Failed to create Product" + e.getMessage();
		} catch (RepositoryException e) {
			
			return "Failed to create Product" + e.getMessage();
		}
		return "Product has been created successfully";
	}
	
	
	
	
	
	

}
