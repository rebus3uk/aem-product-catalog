package uk.co.three.rebus.three_rebus.core.response;

import java.io.Serializable;

import epc.h3g.com.OfferingsDetails;


public class CatalogResponse  extends BaseResponse implements Serializable {
	private OfferingsDetails offeringsDetailsObj = null;

	public OfferingsDetails getOfferingsDetailsObj() {
		return offeringsDetailsObj;
	}

	public void setOfferingsDetailsObj(OfferingsDetails offeringsDetailsObj) {
		this.offeringsDetailsObj = offeringsDetailsObj;
	}
	
	
}
